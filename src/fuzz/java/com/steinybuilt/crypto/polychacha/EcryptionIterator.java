package com.steinybuilt.crypto.polychacha;

import com.steinybuilt.crypto.Bits;

import java.security.SecureRandom;
import java.util.Iterator;

public class EcryptionIterator implements Iterator<Object[]> {

	static final int BUFF_SIZE = 4;

	final SecureRandom random = new SecureRandom();
	final int numIt, max;

	byte[] key, iv, keyInverse, ivInverse;
	Object[][] buffer;
	int index, bufferIndex;

	public EcryptionIterator(int numIterations, int maxDataSize) {
		this.numIt = numIterations;
		this.max = maxDataSize;

		this.key = Bits.randomArray(ChaCha20.KEY_LENGTH);
		this.iv = Bits.randomArray(ChaCha20.NONCE_LENGTH);
		this.keyInverse = Bits.inverseArray(this.key);
		this.ivInverse = Bits.inverseArray(this.iv);

		this.buffer = loadBuffer();
	}

	@Override
	public boolean hasNext() {
		if(bufferIndex < BUFF_SIZE) {
			return true;
		}

		bufferIndex = 0;
		buffer = loadBuffer();
		return (buffer != null);
	}

	@Override
	public Object[] next() {
		return buffer[bufferIndex++];
	}

	Object[][] loadBuffer() {
		byte[] data;

		if(index < numIt) {
			index++;
			int randomLen = random.nextInt(max);
			data = Bits.randomArray(randomLen);
		} else {
			// iterator's done
			index = 0;
			return null;
		}

		Object[][] ret = new Object[BUFF_SIZE][];
		ret[0] = new Object[] {key, iv, data};
		ret[1] = new Object[] {keyInverse, iv, data};
		ret[2] = new Object[] {key, ivInverse, data};
		ret[3] = new Object[] {keyInverse, ivInverse, data};

		return ret;
	}

}
