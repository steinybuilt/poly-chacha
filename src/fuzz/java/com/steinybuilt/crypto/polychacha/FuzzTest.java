package com.steinybuilt.crypto.polychacha;

import com.steinybuilt.crypto.Bits;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;

public class FuzzTest {

	private static final ReentrantLock lock = new ReentrantLock();

	@AfterSuite
	static void afterSuite() {
		Runtime runtime = Runtime.getRuntime();
		System.out.printf("***%n*** Memory usage after suite: %d%n***%n", runtime.totalMemory() - runtime.freeMemory());
	}

	@DataProvider
	static Object[][] keystream() {
		byte[] key = Bits.randomArray(ChaCha20.KEY_LENGTH);
		byte[] iv = Bits.randomArray(ChaCha20.NONCE_LENGTH);

		int size = 15;
		Object[][] ret = new Object[size][];
		ret[0] = new Object[] {key, iv, 0};
		for(int i = 0 ; i < (size - 1) ; i++) {
			ret[i + 1] = new Object[] {key, iv, (int)Math.pow(2, i)};
		}

		return ret;
	}

	@Test(dataProvider = "keystream")
	public void testKeystream(byte[] key, byte[] iv, int length) {
		byte[] nativeKeystream = new byte[length];

		try(NativeChaCha20 cha = new NativeChaCha20(key)) {
			cha.ivSetup(iv);
			cha.keyStreamBytes(nativeKeystream, nativeKeystream.length);
		}

		ChaCha20 chaCha20 = new ChaCha20(key);
		byte[] javaKeystream = chaCha20.keystream(iv, length);

		String nativeKeystreamHex = Bits.formatByteArray(nativeKeystream);
		String javaKeystreamHex = Bits.formatByteArray(javaKeystream);
		Assert.assertTrue(
				Bits.equals(nativeKeystream, javaKeystream),
				String.format("Difference:\nnativeKeystream [%s]\njavaKeystream   [%s]",
						nativeKeystreamHex, javaKeystreamHex)
		);
	}

	@DataProvider
	static Iterator<Object[]> ecryption() {
		return new EcryptionIterator(25, 1024 * 1024);
	}

	@Test(dataProvider = "ecryption")
	public void testEncryption(byte[] key, byte[] iv, byte[] plaintext) {
		int length = plaintext.length;
		int streamLen = 1;

		byte[] nativeKeystream = new byte[streamLen];
		byte[] nativeCipher = new byte[length];

		try(NativeChaCha20 djb = new NativeChaCha20(key)) {
			djb.ivSetup(iv);
			djb.keyStreamBytes(nativeKeystream, nativeKeystream.length);
			djb.encryptBytes(plaintext, nativeCipher, length);
		}

		ChaCha20 chaCha20 = new ChaCha20(key);
		byte[] javaCipher = chaCha20.encryptBytes(iv, plaintext);

		String nativeCipherHex = Bits.formatByteArray(nativeCipher);
		String javaCipherHex = Bits.formatByteArray(javaCipher);
		Assert.assertTrue(
				Bits.equals(nativeCipher, javaCipher),
				String.format("Difference:\nnativeCipher [%s]\njavaCipher   [%s]",
						nativeCipherHex, javaCipherHex)
		);
	}

	@Test(dataProvider = "ecryption")
	public void testParallelEncryption(byte[] key, byte[] iv, byte[] plaintext) {
		int length = plaintext.length;
		int streamLen = 1;

		byte[] nativeKeystream = new byte[streamLen];
		byte[] nativeCipher = new byte[length];

		try(NativeChaCha20 djb = new NativeChaCha20(key)) {
			djb.ivSetup(iv);
			djb.keyStreamBytes(nativeKeystream, nativeKeystream.length);
			djb.encryptBytes(plaintext, nativeCipher, length);
		}

		ChaCha20 chaCha20 = new ChaCha20(key);
		byte[] javaCipher = chaCha20.encryptBytesParallel(iv, plaintext);

		String nativeCipherHex = Bits.formatByteArray(nativeCipher);
		String javaCipherHex = Bits.formatByteArray(javaCipher);
		Assert.assertTrue(
				Bits.equals(nativeCipher, javaCipher),
				String.format("Difference:\nnativeCipher [%s]\njavaCipher   [%s]",
						nativeCipherHex, javaCipherHex)
		);
	}

	@Test(dataProvider = "ecryption")
	public void testDecryption(byte[] key, byte[] iv, byte[] ciphertext) {
		int length = ciphertext.length;
		int streamLen = 1;

		byte[] nativeKeystream = new byte[streamLen];
		byte[] nativePlain = new byte[length];

		try(NativeChaCha20 djb = new NativeChaCha20(key)) {
			djb.ivSetup(iv);
			djb.keyStreamBytes(nativeKeystream, nativeKeystream.length);
			djb.decryptBytes(nativePlain, ciphertext, length);
		}

		ChaCha20 chaCha20 = new ChaCha20(key);
		byte[] javaPlain = chaCha20.encryptBytes(iv, ciphertext);

		String nativePlainHex = Bits.formatByteArray(nativePlain);
		String javaPlainHex = Bits.formatByteArray(javaPlain);
		Assert.assertTrue(
				Bits.equals(nativePlain, javaPlain),
				String.format("Difference:\nnativePlain [%s]\njavaPlain   [%s]",
						nativePlainHex, javaPlainHex)
		);
	}

	@DataProvider
	static Object[][] authentication() {
		byte[] key;
		synchronized(lock) {
			key = Bits.randomArray(Poly1305.KEY_LENGTH);
		}

		int size = 15;
		Object[][] ret = new Object[size][];
		ret[0] = new Object[] {key, new byte[0]};
		for(int i = 0 ; i < (size - 1) ; i++) {
			synchronized(lock) {
				ret[i + 1] = new Object[] {key, Bits.randomArray((int)Math.pow(2, i))};
			}
		}

		return ret;
	}

	@Test(dataProvider = "authentication")
	public void testAuthenticate(byte[] key, byte[] input) {
		byte[] nativeMac = new byte[Poly1305.MAC_LENGTH];

		NativePoly1305 nativePoly = new NativePoly1305(key);
		int i = nativePoly.cryptoOnetimeauthPoly130553(nativeMac, input, input.length);
		Assert.assertEquals(i, 0);

		Poly1305 poly = new Poly1305(key);
		poly.update(input);
		byte[] javaMac = poly.finish();

		String nativeMacHex = Bits.formatByteArray(nativeMac);
		String javaMacHex = Bits.formatByteArray(javaMac);
		Assert.assertTrue(
				Bits.equals(nativeMac, javaMac),
				String.format("Difference:\nnativeMAC [%s]\njavaMAC   [%s]",
						nativeMacHex, javaMacHex)
		);
	}

}
