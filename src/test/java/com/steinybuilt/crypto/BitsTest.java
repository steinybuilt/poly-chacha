package com.steinybuilt.crypto;

import org.testng.annotations.Test;

import java.util.Arrays;

import static com.steinybuilt.crypto.Bits.*;
import static org.testng.Assert.*;

public class BitsTest {

//	final byte[] bytes = new byte[] {
//			(byte)0xff, (byte)0xef, (byte)0xdf, (byte)0xcf,
//			(byte)0xbf, (byte)0xaf, (byte)0x9f, (byte)0x8f
//	};
//	static final byte[] LONG_BE = new byte[]{
//			(byte)0x00, (byte)0x01, (byte)0x02, (byte)0x03,
//			(byte)0x10, (byte)0x11, (byte)0x12, (byte)0x13
//	};

	static final long LONG = 0x00_01_02_03_10_11_12_13L;
	static final byte[] LONG_LE = {0x13, 0x12, 0x11, 0x10, 0x03, 0x02, 0x01, 0x00};

//	static final int INT = 0x00_01_02_03;
//	static final byte[] INT_BE = new byte[]{
//			(byte)0x00, (byte)0x01, (byte)0x02, (byte)0x03
//	};
//	static final byte[] INT_LE = new byte[]{
//			(byte)0x03, (byte)0x02, (byte)0x01, (byte)0x00
//	};

//	@Test
//	public void testLongToBEBytes() {
//		assertEquals(Bits.longToBEBytes(LONG), LONG_BE);
//
//		byte[] shiftedBe = new byte[]{
//				LONG_BE[0], LONG_BE[1], LONG_BE[2], LONG_BE[3]
//		};
//		byte[] output = new byte[4];
//		Bits.longToBEBytes(output, 0, 4, LONG >>> 32);
//		assertEquals(output, shiftedBe);
//	}
//
//	@Test
//	public void testIntToBEBytes() {
//	}

	@Test
	public void testLongToLEBytes() {
		byte[] output = new byte[8];
		longToLEBytes(output, 0, LONG);
		assertEquals(output, LONG_LE);
	}

//	@Test
//	public void testBeBytesToLong() {
//		assertEquals(Bits.beBytesToLong(LONG_BE, 0), LONG);
//		assertEquals(Bits.beBytesToLong(LONG_BE, 0, 4), LONG >>> 32);
//	}

	@Test
	public void testLeBytesToLong() {
	    assertEquals(leBytesToLong(LONG_LE, 0), LONG);
	}

//	@Test
//	public void testBeBytesToInt() {
//		assertEquals(Bits.beBytesToInt(INT_BE, 0), INT);
//		assertEquals(Bits.beBytesToInt(INT_BE, 0, 2), INT >>> 16);
//	}

	@Test
	public void testFormatByteArray() {
		assertEquals(formatByteArray(LONG_LE), "13 12 11 10 03 02 01 00");
	}

	@Test
	public void testEquals_True() {
		byte[] copy = Arrays.copyOf(LONG_LE, LONG_LE.length);
		assertTrue(Bits.equals(copy, LONG_LE));
	}

	@Test
	public void testEquals_False() {
		byte[] copy = new byte[LONG_LE.length];
		for(int i = 0, j = LONG_LE.length - 1 ; i < copy.length ; i++, j--) {
			copy[i] = LONG_LE[j];
		}
		assertFalse(Bits.equals(copy, LONG_LE));
	}

	@Test
	public void testEquals_False_Length() {
		byte[] copy = Arrays.copyOf(LONG_LE, LONG_LE.length -1);
		assertFalse(Bits.equals(copy, LONG_LE));
	}

	@Test
	public void testClear() {
		byte[] bytes = new byte[20];
		for(byte i = 0 ; i < bytes.length ; i++) {
			bytes[i] = i;
		}
		clear(bytes);
		for(byte b : bytes) {
			assertEquals(b, 0);
		}
	}

	@Test
	public void testInverseArray() {
	    byte[] bytes = new byte[] {
			    (byte)0b00000000,
			    (byte)0b11110000,
			    (byte)0b10101010,
			    (byte)0b11100011
	    };
	    byte[] invBytes = new byte[] {
			    (byte)0b11111111,
			    (byte)0b00001111,
			    (byte)0b01010101,
			    (byte)0b00011100
	    };

	    byte[] result = Bits.inverseArray(bytes);
	    assertEquals(result, invBytes);

		result = Bits.inverseArray(invBytes);
		assertEquals(result, bytes);
	}

}
