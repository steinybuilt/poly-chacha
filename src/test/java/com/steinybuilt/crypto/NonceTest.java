package com.steinybuilt.crypto;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.xml.bind.DatatypeConverter;
import java.util.Arrays;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class NonceTest {

	@Test(enabled = false)
	public void develop() {
		long epoch = System.currentTimeMillis();
		System.out.println(Long.toString(epoch));
		System.out.println(Long.toHexString(epoch));
		System.out.println(Long.toBinaryString(epoch));

		byte[] bytes = new byte[8];
		Bits.longToLEBytes(bytes, 0, epoch);
		System.out.println(DatatypeConverter.printHexBinary(bytes));

		System.out.println();

		System.out.println(Long.toString(epoch * 1_000_000L));
		System.out.println(Long.toHexString(epoch * 1_000_000L));
		System.out.println(Long.toBinaryString(epoch * 1_000_000L));

		Bits.longToLEBytes(bytes, 0, epoch * 1_000_000L);
		System.out.println(DatatypeConverter.printHexBinary(bytes));

		System.out.println();

		System.out.println(Long.toString(epoch * 10_000_000L));
		System.out.println(Long.toUnsignedString(epoch * 10_000_000L));
		System.out.println(Long.toHexString(epoch * 10_000_000L));
		System.out.println(Long.toBinaryString(epoch * 10_000_000L));

		Bits.longToLEBytes(bytes, 0, epoch * 10_000_000L);
		System.out.println(DatatypeConverter.printHexBinary(bytes));

		System.out.println();

		long seed = epoch << 24;
		System.out.println(Long.toString(seed));
		System.out.println(Long.toHexString(seed));
		System.out.println(Long.toBinaryString(seed));

		Bits.longToLEBytes(bytes, 0, seed * 1_000_000L);
		System.out.println(DatatypeConverter.printHexBinary(bytes));
	}

	@Test(enabled = false, invocationCount = 200)
	public void testGetNextSeed_Manual() {
		byte[] instId = {0x03, 0x02, 0x01};
		System.out.println(Bits.formatByteArray(Nonce.getNextNonce(instId)));
	}

	@Test
	public void testStaticMember() throws Exception {
	    long before, at, after;
	    byte[] nonce;

	    before = System.currentTimeMillis();
	    Thread.sleep(100);
	    nonce = Nonce.getNextNonce();
		Thread.sleep(100);
	    after = System.currentTimeMillis();

		at = Bits.leBytesToLong(nonce, 4) / 1_000_000L;

		System.out.printf("before = %d%nat     = %d%nafter  = %d%n", before, at, after);
	}

	@Test
	public void testGetNextSeed() {
		byte[] first = Nonce.getNextNonce();
		assertNotNull(first);
		byte[] second = Nonce.getNextNonce();
		assertNotNull(second);
		long firstPlusOne = Bits.leBytesToLong(first, 4) + 1L;
		assertEquals(Bits.leBytesToLong(second, 4), firstPlusOne);
	}

	@Test
	public void testGetNextSeed_InstId() {
		byte[] instId = new byte[Nonce.INSTANCE_ID_LENGTH];
		Bits.intToLEBytes(instId, 0, "hostname".hashCode());

		byte[] first = Nonce.getNextNonce(instId);
		assertNotNull(first);

		assertEquals(first[0], instId[0]);
		assertEquals(first[1], instId[1]);
		assertEquals(first[2], instId[2]);
		assertEquals(first[3], instId[3]);

		byte[] second = Nonce.getNextNonce(instId);
		assertNotNull(second);

		assertEquals(second[0], instId[0]);
		assertEquals(second[1], instId[1]);
		assertEquals(second[2], instId[2]);
		assertEquals(second[3], instId[3]);

		long firstPlusOne = Bits.leBytesToLong(first, 4) + 1L;
		assertEquals(Bits.leBytesToLong(second, 4), firstPlusOne);
	}

	@DataProvider
	Object[][] instanceIds() {
		return new Object[][] {
				{null},
				{new byte[0]},
				{new byte[] {0x05}},
				{new byte[] {0x05, 0x04, 0x03, 0x02}},
				{new byte[] {0x05, 0x04, 0x03, 0x02, 0x01}},
		};
	}

	@Test(dataProvider = "instanceIds")
	public void testInstId(byte[] instId) {
	    byte[] check = instId == null ? new byte[4] : Arrays.copyOf(instId, 4);
		byte[] nonce = Nonce.getNextNonce(instId);
		assertEquals(nonce[0], check[0]);
		assertEquals(nonce[1], check[1]);
		assertEquals(nonce[2], check[2]);
		assertEquals(nonce[3], check[3]);
	}

}
