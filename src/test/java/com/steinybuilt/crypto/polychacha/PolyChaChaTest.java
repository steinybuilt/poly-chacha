package com.steinybuilt.crypto.polychacha;

import com.steinybuilt.crypto.Bits;
import com.steinybuilt.crypto.Nonce;
import org.testng.annotations.Test;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.testng.Assert.*;

public class PolyChaChaTest {


	private final byte[] plaintext = "Ladies and Gentlemen of the class of '99: If I could offer you only one tip for the future, sunscreen would be it.".getBytes(StandardCharsets.US_ASCII);
	private final byte[] aad = {0x50, 0x51, 0x52, 0x53, (byte)0xc0, (byte)0xc1, (byte)0xc2, (byte)0xc3, (byte)0xc4, (byte)0xc5, (byte)0xc6, (byte)0xc7};
	private final byte[] key = {
			(byte)0x80, (byte)0x81, (byte)0x82, (byte)0x83, (byte)0x84, (byte)0x85, (byte)0x86, (byte)0x87, (byte)0x88, (byte)0x89, (byte)0x8a, (byte)0x8b, (byte)0x8c, (byte)0x8d, (byte)0x8e, (byte)0x8f,
			(byte)0x90, (byte)0x91, (byte)0x92, (byte)0x93, (byte)0x94, (byte)0x95, (byte)0x96, (byte)0x97, (byte)0x98, (byte)0x99, (byte)0x9a, (byte)0x9b, (byte)0x9c, (byte)0x9d, (byte)0x9e, (byte)0x9f
	};
	private final byte[] nonce = {0x07, 0x00, 0x00, 0x00, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47};
	private final byte[] mac = {0x1a, (byte)0xe1, 0x0b, 0x59, 0x4f, 0x09, (byte)0xe2, 0x6a, 0x7e, (byte)0x90, 0x2e, (byte)0xcb, (byte)0xd0, 0x60, 0x06, (byte)0x91};
	private final byte[] ciphertext = {
			(byte)0xd3, (byte)0x1a, (byte)0x8d, (byte)0x34, (byte)0x64, (byte)0x8e, (byte)0x60, (byte)0xdb, (byte)0x7b, (byte)0x86, (byte)0xaf, (byte)0xbc, (byte)0x53, (byte)0xef, (byte)0x7e, (byte)0xc2,
			(byte)0xa4, (byte)0xad, (byte)0xed, (byte)0x51, (byte)0x29, (byte)0x6e, (byte)0x08, (byte)0xfe, (byte)0xa9, (byte)0xe2, (byte)0xb5, (byte)0xa7, (byte)0x36, (byte)0xee, (byte)0x62, (byte)0xd6,
			(byte)0x3d, (byte)0xbe, (byte)0xa4, (byte)0x5e, (byte)0x8c, (byte)0xa9, (byte)0x67, (byte)0x12, (byte)0x82, (byte)0xfa, (byte)0xfb, (byte)0x69, (byte)0xda, (byte)0x92, (byte)0x72, (byte)0x8b,
			(byte)0x1a, (byte)0x71, (byte)0xde, (byte)0x0a, (byte)0x9e, (byte)0x06, (byte)0x0b, (byte)0x29, (byte)0x05, (byte)0xd6, (byte)0xa5, (byte)0xb6, (byte)0x7e, (byte)0xcd, (byte)0x3b, (byte)0x36,
			(byte)0x92, (byte)0xdd, (byte)0xbd, (byte)0x7f, (byte)0x2d, (byte)0x77, (byte)0x8b, (byte)0x8c, (byte)0x98, (byte)0x03, (byte)0xae, (byte)0xe3, (byte)0x28, (byte)0x09, (byte)0x1b, (byte)0x58,
			(byte)0xfa, (byte)0xb3, (byte)0x24, (byte)0xe4, (byte)0xfa, (byte)0xd6, (byte)0x75, (byte)0x94, (byte)0x55, (byte)0x85, (byte)0x80, (byte)0x8b, (byte)0x48, (byte)0x31, (byte)0xd7, (byte)0xbc,
			(byte)0x3f, (byte)0xf4, (byte)0xde, (byte)0xf0, (byte)0x8e, (byte)0x4b, (byte)0x7a, (byte)0x9d, (byte)0xe5, (byte)0x76, (byte)0xd2, (byte)0x65, (byte)0x86, (byte)0xce, (byte)0xc6, (byte)0x4b,
			(byte)0x61, (byte)0x16
	};

	@Test
	public void testGenerateKey() {
		byte[] key = PolyChaCha.generateKey();
		assertNotNull(key);
		assertEquals(key.length, ChaCha20.KEY_LENGTH);
		System.out.println(Bits.formatByteArray(key));
	}

	@Test(enabled = false, invocationCount = 4)
	public void testEncrypt_Manual() {
		byte[] key = PolyChaCha.generateKey();
		byte[] instId = {0, 1, 2, 3};
		PolyChaCha pcc = new PolyChaCha(key);
		Encryption encryption = pcc.encrypt(Nonce.getNextNonce(instId), key, key);
		System.out.println(Bits.formatByteArray(encryption.ciphertext));
	}

	@Test
	public void testEncrypt() {
		PolyChaCha pcc = new PolyChaCha(key);
		Encryption encrypt = pcc.encrypt(nonce, plaintext, aad);
		assertEquals(encrypt.nonce, nonce);
		assertEquals(encrypt.aad, aad);
		assertEquals(encrypt.ciphertext, ciphertext);
		assertEquals(encrypt.mac, mac);
	}

	@Test
	public void testEncrypt_NoAad() {
		PolyChaCha pcc = new PolyChaCha(key);
		Encryption encrypt = pcc.encrypt(nonce, plaintext);
		byte[] decrypted = pcc.decrypt(encrypt);
		assertEquals(decrypted, plaintext);
	}

	@Test
	public void testDecrypt() {
		PolyChaCha pcc = new PolyChaCha(key);
		Encryption encrypt = new Encryption(nonce, aad, ciphertext, mac);
		byte[] decrypted = pcc.decrypt(encrypt);
		assertEquals(decrypted, plaintext);
	}

	@Test
	public void testDecryptString() {
		String expected = "com.steinybuilt.crypto.polychacha.PolyChaChaTest.testDecryptString";
		PolyChaCha pcc = new PolyChaCha(key);
		Encryption encrypt = pcc.encrypt(nonce, expected);
		String decrypted = pcc.decryptString(encrypt);
		assertEquals(decrypted, expected);
	}

	@Test(invocationCount = 20)
	public void testParallelEncrypt() {
	    char[] str = new char[104729];
		Arrays.fill(str, 'A');
		String expected = new String(str);

		PolyChaCha pcc = new PolyChaCha(key);
		Encryption encrypt = pcc.encryptParallel(nonce, expected);
		String decrypted = pcc.decryptString(encrypt);
		assertEquals(decrypted, expected);
	}

	@Test(invocationCount = 20)
	public void testParallelDecrypt() {
	    char[] str = new char[104729];
		Arrays.fill(str, 'A');
		String expected = new String(str);

		PolyChaCha pcc = new PolyChaCha(key);
		Encryption encrypt = pcc.encrypt(nonce, expected);
		String decrypted = pcc.decryptStringParallel(encrypt);
		assertEquals(decrypted, expected);
	}

}
