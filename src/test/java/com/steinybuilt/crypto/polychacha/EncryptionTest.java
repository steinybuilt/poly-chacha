package com.steinybuilt.crypto.polychacha;

import org.testng.annotations.Test;

import java.util.Arrays;

import static com.steinybuilt.crypto.polychacha.ChaCha20.NONCE_LENGTH;
import static org.testng.Assert.*;

public class EncryptionTest {

	final byte[] nonce = { // ChaCha20.NONCE_LENGTH = 12
			0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11
	};
	final byte[] aad = {0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19};
	final byte[] ciphertext = {0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27};
	final byte[] mac = { // Poly1305.MAC_LENGTH = 16
			0x28, 0x29, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x40, 0x41, 0x42, 0x43
	};
	final Encryption enc = new Encryption(nonce, aad, ciphertext, mac);

	@Test
	public void testToString() {
		assertEquals(enc.toString(), "{" +
				"\"nonce\": \"000102030405060708091011\", " +
				"\"aad\": \"1213141516171819\", " +
				"\"ciphertext\": \"2021222324252627\", " +
				"\"mac\": \"28293031323334353637383940414243\"" +
				"}");
	}

	@Test
	public void testToString_NullAad() {
		assertEquals(new Encryption(enc.nonce, null, enc.ciphertext, enc.mac).toString(), "{" +
				"\"nonce\": \"000102030405060708091011\", " +
				"\"aad\": null, " +
				"\"ciphertext\": \"2021222324252627\", " +
				"\"mac\": \"28293031323334353637383940414243\"" +
				"}");
	}

	@Test
	public void testClone() {
		Encryption clone = enc.clone();

		assertNotSame(clone, enc);
		assertSame(clone.getClass(), enc.getClass());
		assertEquals(clone, enc);

		assertNotSame(clone.nonce, enc.nonce);
		assertNotSame(clone.aad, enc.aad);
		assertNotSame(clone.ciphertext, enc.ciphertext);
		assertNotSame(clone.mac, enc.mac);

		assertEquals(clone.nonce, nonce);
		assertEquals(clone.aad, aad);
		assertEquals(clone.ciphertext, ciphertext);
		assertEquals(clone.mac, mac);
	}

	@Test
	public void testEquals() {
		assertEquals(enc, new Encryption(new byte[NONCE_LENGTH], null, new byte[]{}, mac));
	}

	@Test
	public void testHashCode() {
		assertEquals(enc.hashCode(), Arrays.hashCode(mac));
	}

	@Test
	public void testGetNonce() {
		assertEquals(enc.getNonce(), nonce);
	}

	@Test
	public void testGetAad() {
		assertEquals(enc.getAad(), aad);
	}

	@Test
	public void testGetCiphertext() {
		assertEquals(enc.getCiphertext(), ciphertext);
	}

	@Test
	public void testGetMac() {
		assertEquals(enc.getMac(), mac);
	}

}
