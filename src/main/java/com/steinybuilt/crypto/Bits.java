package com.steinybuilt.crypto;

import java.security.SecureRandom;

public abstract class Bits {

	public static final char[] HEX_CODE = "0123456789abcdef".toCharArray();

	public static void longToLEBytes(byte[] output, int outputOff, long value) {
		output[outputOff] = (byte)(value & 0xffL);
		output[outputOff + 1] = (byte)(value >>> 8 & 0xffL);
		output[outputOff + 2] = (byte)(value >>> 16 & 0xffL);
		output[outputOff + 3] = (byte)(value >>> 24 & 0xffL);
		output[outputOff + 4] = (byte)(value >>> 32 & 0xffL);
		output[outputOff + 5] = (byte)(value >>> 40 & 0xffL);
		output[outputOff + 6] = (byte)(value >>> 48 & 0xffL);
		output[outputOff + 7] = (byte)(value >>> 56 & 0xffL);
	}

	public static void intToLEBytes(byte[] output, int outputOff, int value) {
		output[outputOff] = (byte)(value & 0xff);
		output[outputOff + 1] = (byte)(value >>> 8 & 0xff);
		output[outputOff + 2] = (byte)(value >>> 16 & 0xff);
		output[outputOff + 3] = (byte)(value >>> 24 & 0xff);
	}

	public static long leBytesToLong(byte[] input, int inputOff) {
		long lng = (input[inputOff + 7] & 0xffL);
		lng = (lng << 8) | (input[inputOff + 6] & 0xffL);
		lng = (lng << 8) | (input[inputOff + 5] & 0xffL);
		lng = (lng << 8) | (input[inputOff + 4] & 0xffL);
		lng = (lng << 8) | (input[inputOff + 3] & 0xffL);
		lng = (lng << 8) | (input[inputOff + 2] & 0xffL);
		lng = (lng << 8) | (input[inputOff + 1] & 0xffL);
		lng = (lng << 8) | (input[inputOff] & 0xffL);
		return lng;
	}

	public static String formatByteArray(byte[] arr) {
		StringBuilder buf = new StringBuilder(arr.length * 3);
		if(arr.length != 0) {
			byte b = arr[0];
			buf.append(HEX_CODE[(b >> 4) & 0xF]).append(HEX_CODE[(b & 0xF)]);
		}
		for(int i = 1 ; i < arr.length ; i++) {
			byte b = arr[i];
			buf.append(' ').append(HEX_CODE[(b >> 4) & 0xF]).append(HEX_CODE[(b & 0xF)]);
		}
		return buf.toString();
	}

	public static boolean equals(byte[] x, byte[] y) {
		int d = 0;

		if(x.length != y.length) {
			return false;
		}

		for(int i = 0 ; i < x.length ; i++) {
			d |= (x[i] & 0xff) ^ (y[i] & 0xff);
		}

		d = (d - 1) >>> 8;
		return (d != 0);
	}

	public static void clear(byte[] bytes) {
		for(int i = 0 ; i < bytes.length ; i++) {
			bytes[i] = (byte)0;
		}
	}

	public static byte[] randomArray(int len) {
		SecureRandom random = new SecureRandom();
		byte[] array = new byte[len];
		random.nextBytes(array);
		return array;
	}

	public static byte[] inverseArray(byte[] array) {
		byte[] inverse = new byte[array.length];
		for(int i = 0 ; i < inverse.length ; i++) {
			inverse[i] = (byte)(~array[i] & 0xFF);
		}
		return inverse;
	}

}
