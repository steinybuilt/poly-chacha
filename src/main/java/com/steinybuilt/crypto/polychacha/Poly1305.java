package com.steinybuilt.crypto.polychacha;

import com.steinybuilt.crypto.Bits;

final class Poly1305 {

	public static final int KEY_LENGTH = 32;
	public static final int MAC_LENGTH = 16;

	//	static final double poly1305_53_constants[] = {
	private static final double constants[] = {
			0.00000000558793544769287109375 /* alpham80 = 3 2^(-29) */
			, 24.0 /* alpham48 = 3 2^3 */
			, 103079215104.0 /* alpham16 = 3 2^35 */
			, 6755399441055744.0 /* alpha0 = 3 2^51 */
			, 1770887431076116955136.0 /* alpha18 = 3 2^69 */
			, 29014219670751100192948224.0 /* alpha32 = 3 2^83 */
			, 7605903601369376408980219232256.0 /* alpha50 = 3 2^101 */
			, 124615124604835863084731911901282304.0 /* alpha64 = 3 2^115 */
			, 32667107224410092492483962313449748299776.0 /* alpha82 = 3 2^133 */
			, 535217884764734955396857238543560676143529984.0 /* alpha96 = 3 2^147 */
			, 35076039295941670036888435985190792471742381031424.0 /* alpha112 = 3 2^163 */
			, 9194973245195333150150082162901855101712434733101613056.0 /* alpha130 = 3 2^181 */
			, 0.0000000000000000000000000000000000000036734198463196484624023016788195177431833298649127735047148490821200539357960224151611328125 /* scale = 5 2^(-130) */
			, 6755408030990331.0 /* offset0 = alpha0 + 2^33 - 5 */
			, 29014256564239239022116864.0 /* offset1 = alpha32 + 2^65 - 2^33 */
			, 124615283061160854719918951570079744.0 /* offset2 = alpha64 + 2^97 - 2^65 */
			, 535219245894202480694386063513315216128475136.0 /* offset3 = alpha96 + 2^130 - 2^97 */
	};

//	private static final byte[] constants = {
//			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x3e // alpham80 = 3 2^(-29)
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x40 // alpham48 = 3 2^3
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x42 // alpham16 = 3 2^35
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x43 // alpha0 = 3 2^51
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x44 // alpha18 = 3 2^69
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x45 // alpha32 = 3 2^83
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x46 // alpha50 = 3 2^101
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x47 // alpha64 = 3 2^115
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x48 // alpha82 = 3 2^133
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x49 // alpha96 = 3 2^147
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x4a // alpha112 = 3 2^163
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x58, 0x4b // alpha130 = 3 2^181
//			, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, (byte)0xf4, 0x37 // scale = 5 2^(-130)
//			, (byte)0xfb, (byte)0xff, (byte)0xff, (byte)0xff, 0x01, 0x00, 0x38, 0x43 // offset0 = alpha0 + 2^33 - 5
//			, (byte)0xfe, (byte)0xff, (byte)0xff, (byte)0xff, 0x01, 0x00, 0x38, 0x45 // offset1 = alpha32 + 2^65 - 2^33
//			, (byte)0xfe, (byte)0xff, (byte)0xff, (byte)0xff, 0x01, 0x00, 0x38, 0x47 // offset2 = alpha64 + 2^97 - 2^65
//			, (byte)0xfe, (byte)0xff, (byte)0xff, (byte)0xff, 0x03, 0x00, 0x38, 0x49 // offset3 = alpha96 + 2^130 - 2^97
//	};

	private final byte[] key;

	double scale;

	double r0high_stack, r0low_stack;
	double r1high_stack, r1low_stack;
	double r2high_stack, r2low_stack;
	double r3high_stack, r3low_stack;

	double sr1high_stack, sr1low_stack;
	double sr2high_stack, sr2low_stack;
	double sr3high_stack, sr3low_stack;

	double r0high, r0low;
	double r1high, r1low;
	double r2high, r2low;
	double r3high, r3low;
	double sr1high, sr1low;
	double sr2high, sr2low;
	double sr3high, sr3low;

	double alpha0, alpha32, alpha64, alpha96, alpha130;
	double h0, h1, h2, h3, h4, h5, h6, h7;
	double y0, y1, y2, y3, y4, y5, y6, y7;
	double x0, x1, x2, x3, x4, x5, x6, x7;

	long d0, d1, d2, d3;

	public Poly1305(byte[] key) {
		double alpham80, alpham48, alpham16;
		double alpha18, alpha50, alpha82, alpha112;
		long r0, r1, r2, r3;
		int r00, r01, r02, r03, r10, r11, r12, r13, r20, r21, r22, r23, r30, r31, r32, r33; // unsigned

		if(key == null) {
			throw new NullPointerException("key cannot be null");
		}
		if(key.length != KEY_LENGTH) {
			throw new IllegalArgumentException("key must be " + KEY_LENGTH + " bytes");
		}

		this.key = key;

		////////////////////////////////////////////////////////////////

		r00 = key[0] & 0xff; // r00 = *(uchar *) (r + 0);
		// constants = (char *) &poly1305_53_constants;

		r01 = key[1] & 0xff; // r01 = *(uchar *) (r + 1);

		r02 = key[2] & 0xff; // r02 = *(uchar *) (r + 2);
		r0 = 2151L;

		r03 = key[3] & 0x0f; // r03 = *(uchar *) (r + 3); and clamp
		r0 <<= 51L;

		r10 = key[4] & 0xfc; // r10 = *(uchar *) (r + 4); and clamp
		r01 <<= 8;
		r0 += r00 & 0xffffffffL;

		r11 = key[5] & 0xff; // r11 = *(uchar *) (r + 5);
		r02 <<= 16;
		r0 += r01 & 0xffffffffL;

		r12 = key[6] & 0xff; // r12 = *(uchar *) (r + 6);
		r03 <<= 24;
		r0 += r02 & 0xffffffffL;

		r13 = key[7] & 0x0f; // r13 = *(uchar *) (r + 7); and clamp
		r1 = 2215L;
		r0 += r03 & 0xffffffffL;

		d0 = r0;
		r1 <<= 51L;
		r2 = 2279L;

		r20 = key[8] & 0xfc; // r20 = *(uchar *) (r + 8); and clamp
		r11 <<= 8;
		r1 += r10 & 0xffffffffL;

		r21 = key[9] & 0xff; // r21 = *(uchar *) (r + 9);
		r12 <<= 16;
		r1 += r11 & 0xffffffffL;

		r22 = key[10] & 0xff; // r22 = *(uchar *) (r + 10);
		r13 <<= 24;
		r1 += r12 & 0xffffffffL;

		r23 = key[11] & 0x0f; // r23 = *(uchar *) (r + 11); and clamp
		r2 <<= 51L;
		r1 += r13 & 0xffffffffL;

		d1 = r1;
		r21 <<= 8;
		r2 += r20 & 0xffffffffL;

		r30 = key[12] & 0xfc; // r30 = *(uchar *) (r + 12); and clamp
		r22 <<= 16;
		r2 += r21 & 0xffffffffL;

		r31 = key[13] & 0xff; // r31 = *(uchar *) (r + 13);
		r23 <<= 24;
		r2 += r22 & 0xffffffffL;

		r32 = key[14] & 0xff; // r32 = *(uchar *) (r + 14);
		r2 += r23 & 0xffffffffL;
		r3 = 2343L;

		d2 = r2;
		r3 <<= 51L;
		alpha32 = constants[5]; // alpha32 = *(double *) (constants + 40);

		r33 = key[15] & 0x0f; // r33 = *(uchar *) (r + 15); and clamp
		r31 <<= 8;
		r3 += r30 & 0xffffffffL;

		r32 <<= 16;
		r3 += r31 & 0xffffffffL;

		r33 <<= 24;
		r3 += r32 & 0xffffffffL;

		r3 += r33 & 0xffffffffL;
		h0 = alpha32 - alpha32;

		d3 = r3;
		h1 = alpha32 - alpha32;

		alpha0 = constants[3]; // alpha0 = *(double *) (constants + 24);
		h2 = alpha32 - alpha32;

		alpha64 = constants[7]; // alpha64 = *(double *) (constants + 56);
		h3 = alpha32 - alpha32;

		alpha18 = constants[4]; // alpha18 = *(double *) (constants + 32);
		h4 = alpha32 - alpha32;

		r0low = Double.longBitsToDouble(d0); // r0low = *(double *) &d0;
		h5 = alpha32 - alpha32;

		r1low = Double.longBitsToDouble(d1); // r1low = *(double *) &d1;
		h6 = alpha32 - alpha32;

		r2low = Double.longBitsToDouble(d2); // r2low = *(double *) &d2;
		h7 = alpha32 - alpha32;

		alpha50 = constants[6]; // alpha50 = *(double *) (constants + 48);
		r0low -= alpha0;

		alpha82 = constants[8]; // alpha82 = *(double *) (constants + 64);
		r1low -= alpha32;

		scale = constants[12]; // scale = *(double *) (constants + 96);
		r2low -= alpha64;

		alpha96 = constants[9]; // alpha96 = *(double *) (constants + 72);
		r0high = r0low + alpha18;

		r3low = Double.longBitsToDouble(d3); // r3low = *(double *) &d3;

		alpham80 = constants[0]; // alpham80 = *(double *) (constants + 0);
		r1high = r1low + alpha50;
		sr1low = scale * r1low;

		alpham48 = constants[1]; // alpham48 = *(double *) (constants + 8);
		r2high = r2low + alpha82;
		sr2low = scale * r2low;

		r0high -= alpha18;
		r0high_stack = r0high;

		r3low -= alpha96;

		r1high -= alpha50;
		r1high_stack = r1high;

		sr1high = sr1low + alpham80;

		alpha112 = constants[10]; // alpha112 = *(double *) (constants + 80);
		r0low -= r0high;

		alpham16 = constants[2]; // alpham16 = *(double *) (constants + 16);
		r2high -= alpha82;
		sr3low = scale * r3low;

		alpha130 = constants[11]; // alpha130 = *(double *) (constants + 88);
		sr2high = sr2low + alpham48;

		r1low -= r1high;
		r1low_stack = r1low;

		sr1high -= alpham80;
		sr1high_stack = sr1high;

		r2low -= r2high;
		r2low_stack = r2low;

		sr2high -= alpham48;
		sr2high_stack = sr2high;

		r3high = r3low + alpha112;
		r0low_stack = r0low;

		sr1low -= sr1high;
		sr1low_stack = sr1low;

		sr3high = sr3low + alpham16;
		r2high_stack = r2high;

		sr2low -= sr2high;
		sr2low_stack = sr2low;

		r3high -= alpha112;
		r3high_stack = r3high;


		sr3high -= alpham16;
		sr3high_stack = sr3high;


		r3low -= r3high;
		r3low_stack = r3low;


		sr3low -= sr3high;
		sr3low_stack = sr3low;

	}

	public static byte[] authenticate(byte[] key, byte[] message) {
		Poly1305 poly = new Poly1305(key);
		poly.poly1305_53(message, message.length);
		return poly.noMoreBytes();
	}

	public final void update(byte[] partial) {
		poly1305_53(partial, partial.length);
	}

	@SuppressWarnings("fallthrough")
	public final void updateAligned(byte[] partial) {
		int remaining = partial.length % 16;
		if(remaining == 0) {
			poly1305_53(partial, partial.length);
		} else {
			int alignment = partial.length - remaining;
			poly1305_53(partial, alignment);

			byte[] lastBlock = new byte[16];
			int offset = partial.length;
			switch(remaining) {
				case 15:
					lastBlock[14] = partial[--offset];
				case 14:
					lastBlock[13] = partial[--offset];
				case 13:
					lastBlock[12] = partial[--offset];
				case 12:
					lastBlock[11] = partial[--offset];
				case 11:
					lastBlock[10] = partial[--offset];
				case 10:
					lastBlock[9] = partial[--offset];
				case 9:
					lastBlock[8] = partial[--offset];
				case 8:
					lastBlock[7] = partial[--offset];
				case 7:
					lastBlock[6] = partial[--offset];
				case 6:
					lastBlock[5] = partial[--offset];
				case 5:
					lastBlock[4] = partial[--offset];
				case 4:
					lastBlock[3] = partial[--offset];
				case 3:
					lastBlock[2] = partial[--offset];
				case 2:
					lastBlock[1] = partial[--offset];
				case 1:
					lastBlock[0] = partial[--offset];
			}
			poly1305_53(lastBlock, 16);
		}
	}

	/**
	 * Complete the MAC calculation.
	 * @return 16-byte Message Authentication Code.
	 */
	public final byte[] finish() {
		return noMoreBytes();
	}

	static boolean equals(byte[] x, byte[] y) {
		if(x == null || y == null || x.length != MAC_LENGTH || y.length != MAC_LENGTH) {
			return false;
		}

		int d = (x[0] & 0xff) ^ (y[0] & 0xff);
		d |= (x[1] & 0xff) ^ (y[1] & 0xff);
		d |= (x[2] & 0xff) ^ (y[2] & 0xff);
		d |= (x[3] & 0xff) ^ (y[3] & 0xff);
		d |= (x[4] & 0xff) ^ (y[4] & 0xff);
		d |= (x[5] & 0xff) ^ (y[5] & 0xff);
		d |= (x[6] & 0xff) ^ (y[6] & 0xff);
		d |= (x[7] & 0xff) ^ (y[7] & 0xff);
		d |= (x[8] & 0xff) ^ (y[8] & 0xff);
		d |= (x[9] & 0xff) ^ (y[9] & 0xff);
		d |= (x[10] & 0xff) ^ (y[10] & 0xff);
		d |= (x[11] & 0xff) ^ (y[11] & 0xff);
		d |= (x[12] & 0xff) ^ (y[12] & 0xff);
		d |= (x[13] & 0xff) ^ (y[13] & 0xff);
		d |= (x[14] & 0xff) ^ (y[14] & 0xff);
		d |= (x[15] & 0xff) ^ (y[15] & 0xff);

		d = (d - 1) >>> 8;
		return (d != 0);
	}

	@SuppressWarnings("SuspiciousNameCombination")
	private void poly1305_53(byte[] m, int l) {
		double r0highx0, r0lowx0;
		double r0highx2, r0lowx2;
		double r0highx4, r0lowx4;
		double r0highx6, r0lowx6;
		double r1highx0, r1lowx0;
		double r1highx2, r1lowx2;
		double r1highx4, r1lowx4;
		double r2highx0, r2lowx0;
		double r2highx2, r2lowx2;
		double r3highx0, r3lowx0;
		double sr1highx6, sr1lowx6;
		double sr2highx4, sr2lowx4;
		double sr2highx6, sr2lowx6;
		double sr3highx2, sr3lowx2;
		double sr3highx4, sr3lowx4;
		double sr3lowx6, sr3highx6;
		double z0, z1, z2, z3;
		long m0, m1, m2, m3;
		int m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32; // unsigned
		long m33; // unsigned
		int mOff;
		int lbelow2, lbelow3, lbelow4, lbelow5, lbelow6, lbelow7, lbelow8, lbelow9, lbelow10, lbelow11, lbelow12, lbelow13, lbelow14, lbelow15;

		mOff = 0;

		//if (l < 16) goto addatmost15bytes;
		if(l >= 16) {

			m00 = m[0] & 0xff; // m00 = *(uchar *) (m + 0);
			m0 = 2151L;

			m0 <<= 51L;
			m1 = 2215L;
			m01 = m[1] & 0xff; // m01 = *(uchar *) (m + 1);

			m1 <<= 51L;
			m2 = 2279L;
			m02 = m[2] & 0xff; // m02 = *(uchar *) (m + 2);

			m2 <<= 51L;
			m3 = 2343L;
			m03 = m[3] & 0xff; // m03 = *(uchar *) (m + 3);

			m10 = m[4] & 0xff; // m10 = *(uchar *) (m + 4);
			m01 <<= 8;
			m0 += m00 & 0xffffffffL;

			m11 = m[5] & 0xff; // m11 = *(uchar *) (m + 5);
			m02 <<= 16;
			m0 += m01 & 0xffffffffL;

			m12 = m[6] & 0xff; // m12 = *(uchar *) (m + 6);
			m03 <<= 24;
			m0 += m02 & 0xffffffffL;

			m13 = m[7] & 0xff; // m13 = *(uchar *) (m + 7);
			m3 <<= 51L;
			m0 += m03 & 0xffffffffL;

			m20 = m[8] & 0xff; // m20 = *(uchar *) (m + 8);
			m11 <<= 8;
			m1 += m10 & 0xffffffffL;

			m21 = m[9] & 0xff; // m21 = *(uchar *) (m + 9);
			m12 <<= 16;
			m1 += m11 & 0xffffffffL;

			m22 = m[10] & 0xff; // m22 = *(uchar *) (m + 10);
			m13 <<= 24;
			m1 += m12 & 0xffffffffL;

			m23 = m[11] & 0xff; // m23 = *(uchar *) (m + 11);
			m1 += m13 & 0xffffffffL;

			m30 = m[12] & 0xff; // m30 = *(uchar *) (m + 12);
			m21 <<= 8;
			m2 += m20 & 0xffffffffL;

			m31 = m[13] & 0xff; // m31 = *(uchar *) (m + 13);
			m22 <<= 16;
			m2 += m21 & 0xffffffffL;

			m32 = m[14] & 0xff; // m32 = *(uchar *) (m + 14);
			m23 <<= 24;
			m2 += m22 & 0xffffffffL;

			m33 = m[15] & 0xffL; // m33 = *(uchar *) (m + 15);
			m2 += m23 & 0xffffffffL;

			d0 = m0;
			m31 <<= 8;
			m3 += m30 & 0xffffffffL;

			d1 = m1;
			m32 <<= 16;
			m3 += m31 & 0xffffffffL;

			d2 = m2;
			m33 += 256L;

			m33 <<= 24;
			m3 += m32 & 0xffffffffL;

			m3 += m33;
			d3 = m3;

			mOff += 16; // m += 16;
			l -= 16;

			z0 = Double.longBitsToDouble(d0); // z0 = *(double *) &d0;

			z1 = Double.longBitsToDouble(d1); // z1 = *(double *) &d1;

			z2 = Double.longBitsToDouble(d2); // z2 = *(double *) &d2;

			z3 = Double.longBitsToDouble(d3); // z3 = *(double *) &d3;

			z0 -= alpha0;

			z1 -= alpha32;

			z2 -= alpha64;

			z3 -= alpha96;

			h0 += z0;

			h1 += z1;

			h3 += z2;

			h5 += z3;

//		    if (l < 16) goto multiplyaddatmost15bytes;
//		    multiplyaddatleast16bytes:;
			while(l >= 16) {

				m2 = 2279;
				m20 = m[mOff + 8] & 0xff; // m20 = *(uchar *) (m + 8);
				y7 = h7 + alpha130;

				m2 <<= 51;
				m3 = 2343;
				m21 = m[mOff + 9] & 0xff; // m21 = *(uchar *) (m + 9);
				y6 = h6 + alpha130;

				m3 <<= 51;
				m0 = 2151;
				m22 = m[mOff + 10] & 0xff; // m22 = *(uchar *) (m + 10);
				y1 = h1 + alpha32;

				m0 <<= 51;
				m1 = 2215;
				m23 = m[mOff + 11] & 0xff; // m23 = *(uchar *) (m + 11);
				y0 = h0 + alpha32;

				m1 <<= 51;
				m30 = m[mOff + 12] & 0xff; // m30 = *(uchar *) (m + 12);
				y7 -= alpha130;

				m21 <<= 8;
				m2 += m20 & 0xffffffffL;
				m31 = m[mOff + 13] & 0xff; // m31 = *(uchar *) (m + 13);
				y6 -= alpha130;

				m22 <<= 16;
				m2 += m21 & 0xffffffffL;
				m32 = m[mOff + 14] & 0xff; // m32 = *(uchar *) (m + 14);
				y1 -= alpha32;

				m23 <<= 24;
				m2 += m22 & 0xffffffffL;
				m33 = m[mOff + 15] & 0xffL; // m33 = *(uchar *) (m + 15);
				y0 -= alpha32;

				m2 += m23 & 0xffffffffL;
				m00 = m[mOff] & 0xff; // m00 = *(uchar *) (m + 0);
				y5 = h5 + alpha96;

				m31 <<= 8;
				m3 += m30 & 0xffffffffL;
				m01 = m[mOff + 1] & 0xff; // m01 = *(uchar *) (m + 1);
				y4 = h4 + alpha96;

				m32 <<= 16;
				m02 = m[mOff + 2] & 0xff; // m02 = *(uchar *) (m + 2);
				x7 = h7 - y7;
				y7 *= scale;

				m33 += 256L;
				m03 = m[mOff + 3] & 0xff; // m03 = *(uchar *) (m + 3);
				x6 = h6 - y6;
				y6 *= scale;

				m33 <<= 24;
				m3 += m31 & 0xffffffffL;
				m10 = m[mOff + 4] & 0xff; // m10 = *(uchar *) (m + 4);
				x1 = h1 - y1;

				m01 <<= 8;
				m3 += m32 & 0xffffffffL;
				m11 = m[mOff + 5] & 0xff; // m11 = *(uchar *) (m + 5);
				x0 = h0 - y0;

				m3 += m33;
				m0 += m00 & 0xffffffffL;
				m12 = m[mOff + 6] & 0xff; // m12 = *(uchar *) (m + 6);
				y5 -= alpha96;

				m02 <<= 16;
				m0 += m01 & 0xffffffffL;
				m13 = m[mOff + 7] & 0xff; // m13 = *(uchar *) (m + 7);
				y4 -= alpha96;

				m03 <<= 24;
				m0 += m02 & 0xffffffffL;
				d2 = m2;
				x1 += y7;

				m0 += m03 & 0xffffffffL;
				d3 = m3;
				x0 += y6;

				m11 <<= 8;
				m1 += m10 & 0xffffffffL;
				d0 = m0;
				x7 += y5;

				m12 <<= 16;
				m1 += m11 & 0xffffffffL;
				x6 += y4;

				m13 <<= 24;
				m1 += m12 & 0xffffffffL;
				y3 = h3 + alpha64;

				m1 += m13 & 0xffffffffL;
				d1 = m1;
				y2 = h2 + alpha64;

				x0 += x1;

				x6 += x7;

				y3 -= alpha64;
				r3low = r3low_stack;

				y2 -= alpha64;
				r0low = r0low_stack;

				x5 = h5 - y5;
				r3lowx0 = r3low * x0;
				r3high = r3high_stack;

				x4 = h4 - y4;
				r0lowx6 = r0low * x6;
				r0high = r0high_stack;

				x3 = h3 - y3;
				r3highx0 = r3high * x0;
				sr1low = sr1low_stack;

				x2 = h2 - y2;
				r0highx6 = r0high * x6;
				sr1high = sr1high_stack;

				x5 += y3;
				r0lowx0 = r0low * x0;
				r1low = r1low_stack;

				h6 = r3lowx0 + r0lowx6;
				sr1lowx6 = sr1low * x6;
				r1high = r1high_stack;

				x4 += y2;
				r0highx0 = r0high * x0;
				sr2low = sr2low_stack;

				h7 = r3highx0 + r0highx6;
				sr1highx6 = sr1high * x6;
				sr2high = sr2high_stack;

				x3 += y1;
				r1lowx0 = r1low * x0;
				r2low = r2low_stack;

				h0 = r0lowx0 + sr1lowx6;
				sr2lowx6 = sr2low * x6;
				r2high = r2high_stack;

				x2 += y0;
				r1highx0 = r1high * x0;
				sr3low = sr3low_stack;

				h1 = r0highx0 + sr1highx6;
				sr2highx6 = sr2high * x6;
				sr3high = sr3high_stack;

				x4 += x5;
				r2lowx0 = r2low * x0;
				z2 = Double.longBitsToDouble(d2); // z2 = *(double *) &d2;

				h2 = r1lowx0 + sr2lowx6;
				sr3lowx6 = sr3low * x6;

				x2 += x3;
				r2highx0 = r2high * x0;
				z3 = Double.longBitsToDouble(d3); // z3 = *(double *) &d3;

				h3 = r1highx0 + sr2highx6;
				sr3highx6 = sr3high * x6;

				r1highx4 = r1high * x4;
				z2 -= alpha64;

				h4 = r2lowx0 + sr3lowx6;
				r1lowx4 = r1low * x4;

				r0highx4 = r0high * x4;
				z3 -= alpha96;

				h5 = r2highx0 + sr3highx6;
				r0lowx4 = r0low * x4;

				h7 += r1highx4;
				sr3highx4 = sr3high * x4;

				h6 += r1lowx4;
				sr3lowx4 = sr3low * x4;

				h5 += r0highx4;
				sr2highx4 = sr2high * x4;

				h4 += r0lowx4;
				sr2lowx4 = sr2low * x4;

				h3 += sr3highx4;
				r0lowx2 = r0low * x2;

				h2 += sr3lowx4;
				r0highx2 = r0high * x2;

				h1 += sr2highx4;
				r1lowx2 = r1low * x2;

				h0 += sr2lowx4;
				r1highx2 = r1high * x2;

				h2 += r0lowx2;
				r2lowx2 = r2low * x2;

				h3 += r0highx2;
				r2highx2 = r2high * x2;

				h4 += r1lowx2;
				sr3lowx2 = sr3low * x2;

				h5 += r1highx2;
				sr3highx2 = sr3high * x2;
				alpha0 = constants[3]; // alpha0 = *(double *) (constants + 24);

				mOff += 16; // m += 16;
				h6 += r2lowx2;

				l -= 16;
				h7 += r2highx2;

				z1 = Double.longBitsToDouble(d1); // z1 = *(double *) &d1;
				h0 += sr3lowx2;

				z0 = Double.longBitsToDouble(d0); // z0 = *(double *) &d0;
				h1 += sr3highx2;

				z1 -= alpha32;

				z0 -= alpha0;

				h5 += z3;

				h3 += z2;

				h1 += z1;

				h0 += z0;

			} // while(l >= 16)

//			if(l >= 16)goto multiplyaddatleast16bytes;
//			multiplyaddatmost15bytes:;

			y7 = h7 + alpha130;

			y6 = h6 + alpha130;

			y1 = h1 + alpha32;

			y0 = h0 + alpha32;

			y7 -= alpha130;

			y6 -= alpha130;

			y1 -= alpha32;

			y0 -= alpha32;

			y5 = h5 + alpha96;

			y4 = h4 + alpha96;

			x7 = h7 - y7;
			y7 *= scale;

			x6 = h6 - y6;
			y6 *= scale;

			x1 = h1 - y1;

			x0 = h0 - y0;

			y5 -= alpha96;

			y4 -= alpha96;

			x1 += y7;

			x0 += y6;

			x7 += y5;

			x6 += y4;

			y3 = h3 + alpha64;

			y2 = h2 + alpha64;

			x0 += x1;

			x6 += x7;

			y3 -= alpha64;
			r3low = r3low_stack;

			y2 -= alpha64;
			r0low = r0low_stack;

			x5 = h5 - y5;
			r3lowx0 = r3low * x0;
			r3high = r3high_stack;

			x4 = h4 - y4;
			r0lowx6 = r0low * x6;
			r0high = r0high_stack;

			x3 = h3 - y3;
			r3highx0 = r3high * x0;
			sr1low = sr1low_stack;

			x2 = h2 - y2;
			r0highx6 = r0high * x6;
			sr1high = sr1high_stack;

			x5 += y3;
			r0lowx0 = r0low * x0;
			r1low = r1low_stack;

			h6 = r3lowx0 + r0lowx6;
			sr1lowx6 = sr1low * x6;
			r1high = r1high_stack;

			x4 += y2;
			r0highx0 = r0high * x0;
			sr2low = sr2low_stack;

			h7 = r3highx0 + r0highx6;
			sr1highx6 = sr1high * x6;
			sr2high = sr2high_stack;

			x3 += y1;
			r1lowx0 = r1low * x0;
			r2low = r2low_stack;

			h0 = r0lowx0 + sr1lowx6;
			sr2lowx6 = sr2low * x6;
			r2high = r2high_stack;

			x2 += y0;
			r1highx0 = r1high * x0;
			sr3low = sr3low_stack;

			h1 = r0highx0 + sr1highx6;
			sr2highx6 = sr2high * x6;
			sr3high = sr3high_stack;

			x4 += x5;
			r2lowx0 = r2low * x0;

			h2 = r1lowx0 + sr2lowx6;
			sr3lowx6 = sr3low * x6;

			x2 += x3;
			r2highx0 = r2high * x0;

			h3 = r1highx0 + sr2highx6;
			sr3highx6 = sr3high * x6;

			r1highx4 = r1high * x4;

			h4 = r2lowx0 + sr3lowx6;
			r1lowx4 = r1low * x4;

			r0highx4 = r0high * x4;

			h5 = r2highx0 + sr3highx6;
			r0lowx4 = r0low * x4;

			h7 += r1highx4;
			sr3highx4 = sr3high * x4;

			h6 += r1lowx4;
			sr3lowx4 = sr3low * x4;

			h5 += r0highx4;
			sr2highx4 = sr2high * x4;

			h4 += r0lowx4;
			sr2lowx4 = sr2low * x4;

			h3 += sr3highx4;
			r0lowx2 = r0low * x2;

			h2 += sr3lowx4;
			r0highx2 = r0high * x2;

			h1 += sr2highx4;
			r1lowx2 = r1low * x2;

			h0 += sr2lowx4;
			r1highx2 = r1high * x2;

			h2 += r0lowx2;
			r2lowx2 = r2low * x2;

			h3 += r0highx2;
			r2highx2 = r2high * x2;

			h4 += r1lowx2;
			sr3lowx2 = sr3low * x2;

			h5 += r1highx2;
			sr3highx2 = sr3high * x2;

			h6 += r2lowx2;

			h7 += r2highx2;

			h0 += sr3lowx2;

			h1 += sr3highx2;

		} // if(l >= 16)

//		addatmost15bytes:;
//		if (l == 0) goto nomorebytes;

		if(l > 0) {

			lbelow2 = l - 2;

			lbelow3 = l - 3;

			lbelow2 >>= 31;
			lbelow4 = l - 4;

			m00 = m[mOff] & 0xff; // m00 = *(uchar *) (m + 0);
			lbelow3 >>= 31;
			mOff += lbelow2; // m += lbelow2;

			m01 = m[mOff + 1] & 0xff; // m01 = *(uchar *) (m + 1);
			lbelow4 >>= 31;
			mOff += lbelow3; // m += lbelow3;

			m02 = m[mOff + 2] & 0xff; // m02 = *(uchar *) (m + 2);
			mOff += lbelow4; // m += lbelow4;
			m0 = 2151;

			m03 = m[mOff + 3] & 0xff; // m03 = *(uchar *) (m + 3);
			m0 <<= 51;
			m1 = 2215;

			m0 += m00 & 0xffffffffL;
			m01 &= ~lbelow2;

			m02 &= ~lbelow3;
			m01 -= lbelow2;

			m01 <<= 8;
			m03 &= ~lbelow4;

			m0 += m01 & 0xffffffffL;
			lbelow2 -= lbelow3;

			m02 += lbelow2;
			lbelow3 -= lbelow4;

			m02 <<= 16;
			m03 += lbelow3;

			m03 <<= 24;
			m0 += m02 & 0xffffffffL;

			m0 += m03 & 0xffffffffL;
			lbelow5 = l - 5;

			lbelow6 = l - 6;
			lbelow7 = l - 7;

			lbelow5 >>= 31;
			lbelow8 = l - 8;

			lbelow6 >>= 31;
			mOff += lbelow5; // m += lbelow5;

			m10 = m[mOff + 4] & 0xff; // m10 = *(uchar *) (m + 4);
			lbelow7 >>= 31;
			mOff += lbelow6; // m += lbelow6;

			m11 = m[mOff + 5] & 0xff; // m11 = *(uchar *) (m + 5);
			lbelow8 >>= 31;
			mOff += lbelow7; // m += lbelow7;

			m12 = m[mOff + 6] & 0xff; // m12 = *(uchar *) (m + 6);
			m1 <<= 51;
			mOff += lbelow8; // m += lbelow8;

			m13 = m[mOff + 7] & 0xff; // m13 = *(uchar *) (m + 7);
			m10 &= ~lbelow5;
			lbelow4 -= lbelow5;

			m10 += lbelow4;
			lbelow5 -= lbelow6;

			m11 &= ~lbelow6;
			m11 += lbelow5;

			m11 <<= 8;
			m1 += m10 & 0xffffffffL;

			m1 += m11 & 0xffffffffL;
			m12 &= ~lbelow7;

			lbelow6 -= lbelow7;
			m13 &= ~lbelow8;

			m12 += lbelow6;
			lbelow7 -= lbelow8;

			m12 <<= 16;
			m13 += lbelow7;

			m13 <<= 24;
			m1 += m12 & 0xffffffffL;

			m1 += m13 & 0xffffffffL;
			m2 = 2279;

			lbelow9 = l - 9;
			m3 = 2343;

			lbelow10 = l - 10;
			lbelow11 = l - 11;

			lbelow9 >>= 31;
			lbelow12 = l - 12;

			lbelow10 >>= 31;
			mOff += lbelow9; // m += lbelow9;

			m20 = m[mOff + 8] & 0xff; // m20 = *(uchar *) (m + 8);
			lbelow11 >>= 31;
			mOff += lbelow10; // m += lbelow10;

			m21 = m[mOff + 9] & 0xff; // m21 = *(uchar *) (m + 9);
			lbelow12 >>= 31;
			mOff += lbelow11; // m += lbelow11;

			m22 = m[mOff + 10] & 0xff; // m22 = *(uchar *) (m + 10);
			m2 <<= 51;
			mOff += lbelow12; // m += lbelow12;

			m23 = m[mOff + 11] & 0xff; // m23 = *(uchar *) (m + 11);
			m20 &= ~lbelow9;
			lbelow8 -= lbelow9;

			m20 += lbelow8;
			lbelow9 -= lbelow10;

			m21 &= ~lbelow10;
			m21 += lbelow9;

			m21 <<= 8;
			m2 += m20 & 0xffffffffL;

			m2 += m21 & 0xffffffffL;
			m22 &= ~lbelow11;

			lbelow10 -= lbelow11;
			m23 &= ~lbelow12;

			m22 += lbelow10;
			lbelow11 -= lbelow12;

			m22 <<= 16;
			m23 += lbelow11;

			m23 <<= 24;
			m2 += m22 & 0xffffffffL;

			m3 <<= 51;
			lbelow13 = l - 13;

			lbelow13 >>= 31;
			lbelow14 = l - 14;

			lbelow14 >>= 31;
			mOff += lbelow13; // m += lbelow13;
			lbelow15 = l - 15;

			m30 = m[mOff + 12] & 0xff; // m30 = *(uchar *) (m + 12);
			lbelow15 >>= 31;
			mOff += lbelow14; // m += lbelow14;

			m31 = m[mOff + 13] & 0xff; // m31 = *(uchar *) (m + 13);
			mOff += lbelow15; // m += lbelow15;
			m2 += m23 & 0xffffffffL;

			m32 = m[mOff + 14] & 0xff; // m32 = *(uchar *) (m + 14);
			m30 &= ~lbelow13;
			lbelow12 -= lbelow13;

			m30 += lbelow12;
			lbelow13 -= lbelow14;

			m3 += m30 & 0xffffffffL;
			m31 &= ~lbelow14;

			m31 += lbelow13;
			m32 &= ~lbelow15;

			m31 <<= 8;
			lbelow14 -= lbelow15;

			m3 += m31 & 0xffffffffL;
			m32 += lbelow14;
			d0 = m0;

			m32 <<= 16;
			m33 = (lbelow15 + 1) & 0xffffffffL;
			d1 = m1;

			m33 <<= 24;
			m3 += m32 & 0xffffffffL;
			d2 = m2;

			m3 += m33;
			d3 = m3;

			alpha0 = constants[3]; // alpha0 = *(double *) (constants + 24);

			z3 = Double.longBitsToDouble(d3); // z3 = *(double *) &d3;

			z2 = Double.longBitsToDouble(d2); // z2 = *(double *) &d2;

			z1 = Double.longBitsToDouble(d1); // z1 = *(double *) &d1;

			z0 = Double.longBitsToDouble(d0); // z0 = *(double *) &d0;

			z3 -= alpha96;

			z2 -= alpha64;

			z1 -= alpha32;

			z0 -= alpha0;

			h5 += z3;

			h3 += z2;

			h1 += z1;

			h0 += z0;

			y7 = h7 + alpha130;

			y6 = h6 + alpha130;

			y1 = h1 + alpha32;

			y0 = h0 + alpha32;

			y7 -= alpha130;

			y6 -= alpha130;

			y1 -= alpha32;

			y0 -= alpha32;

			y5 = h5 + alpha96;

			y4 = h4 + alpha96;

			x7 = h7 - y7;
			y7 *= scale;

			x6 = h6 - y6;
			y6 *= scale;

			x1 = h1 - y1;

			x0 = h0 - y0;

			y5 -= alpha96;

			y4 -= alpha96;

			x1 += y7;

			x0 += y6;

			x7 += y5;

			x6 += y4;

			y3 = h3 + alpha64;

			y2 = h2 + alpha64;

			x0 += x1;

			x6 += x7;

			y3 -= alpha64;
			r3low = r3low_stack;

			y2 -= alpha64;
			r0low = r0low_stack;

			x5 = h5 - y5;
			r3lowx0 = r3low * x0;
			r3high = r3high_stack;

			x4 = h4 - y4;
			r0lowx6 = r0low * x6;
			r0high = r0high_stack;

			x3 = h3 - y3;
			r3highx0 = r3high * x0;
			sr1low = sr1low_stack;

			x2 = h2 - y2;
			r0highx6 = r0high * x6;
			sr1high = sr1high_stack;

			x5 += y3;
			r0lowx0 = r0low * x0;
			r1low = r1low_stack;

			h6 = r3lowx0 + r0lowx6;
			sr1lowx6 = sr1low * x6;
			r1high = r1high_stack;

			x4 += y2;
			r0highx0 = r0high * x0;
			sr2low = sr2low_stack;

			h7 = r3highx0 + r0highx6;
			sr1highx6 = sr1high * x6;
			sr2high = sr2high_stack;

			x3 += y1;
			r1lowx0 = r1low * x0;
			r2low = r2low_stack;

			h0 = r0lowx0 + sr1lowx6;
			sr2lowx6 = sr2low * x6;
			r2high = r2high_stack;

			x2 += y0;
			r1highx0 = r1high * x0;
			sr3low = sr3low_stack;

			h1 = r0highx0 + sr1highx6;
			sr2highx6 = sr2high * x6;
			sr3high = sr3high_stack;

			x4 += x5;
			r2lowx0 = r2low * x0;

			h2 = r1lowx0 + sr2lowx6;
			sr3lowx6 = sr3low * x6;

			x2 += x3;
			r2highx0 = r2high * x0;

			h3 = r1highx0 + sr2highx6;
			sr3highx6 = sr3high * x6;

			r1highx4 = r1high * x4;

			h4 = r2lowx0 + sr3lowx6;
			r1lowx4 = r1low * x4;

			r0highx4 = r0high * x4;

			h5 = r2highx0 + sr3highx6;
			r0lowx4 = r0low * x4;

			h7 += r1highx4;
			sr3highx4 = sr3high * x4;

			h6 += r1lowx4;
			sr3lowx4 = sr3low * x4;

			h5 += r0highx4;
			sr2highx4 = sr2high * x4;

			h4 += r0lowx4;
			sr2lowx4 = sr2low * x4;

			h3 += sr3highx4;
			r0lowx2 = r0low * x2;

			h2 += sr3lowx4;
			r0highx2 = r0high * x2;

			h1 += sr2highx4;
			r1lowx2 = r1low * x2;

			h0 += sr2lowx4;
			r1highx2 = r1high * x2;

			h2 += r0lowx2;
			r2lowx2 = r2low * x2;

			h3 += r0highx2;
			r2highx2 = r2high * x2;

			h4 += r1lowx2;
			sr3lowx2 = sr3low * x2;

			h5 += r1highx2;
			sr3highx2 = sr3high * x2;

			h6 += r2lowx2;

			h7 += r2highx2;

			h0 += sr3lowx2;

			h1 += sr3highx2;

		} // if(l > 0)
	}

	@SuppressWarnings("SuspiciousNameCombination")
	private byte[] noMoreBytes() {
		byte[] out = new byte[MAC_LENGTH];
		double offset0, offset1, offset2, offset3;
		long bits32; // unsigned
		long f, f0, f1, f2, f3, f4; // unsigned
		long g, g0, g1, g2, g3, g4; // unsigned
		int s00, s01, s02, s03, s10, s11, s12, s13, s20, s21, s22, s23, s30, s31, s32, s33; // unsigned

//		nomorebytes:;

		offset0 = constants[13]; // offset0 = *(double *) (constants + 104);
		y7 = h7 + alpha130;

		offset1 = constants[14]; // offset1 = *(double *) (constants + 112);
		y0 = h0 + alpha32;

		offset2 = constants[15]; // offset2 = *(double *) (constants + 120);
		y1 = h1 + alpha32;

		offset3 = constants[16]; // offset3 = *(double *) (constants + 128);
		y2 = h2 + alpha64;

		y7 -= alpha130;

		y3 = h3 + alpha64;

		y4 = h4 + alpha96;

		y5 = h5 + alpha96;

		x7 = h7 - y7;
		y7 *= scale;

		y0 -= alpha32;

		y1 -= alpha32;

		y2 -= alpha64;

		h6 += x7;

		y3 -= alpha64;

		y4 -= alpha96;

		y5 -= alpha96;

		y6 = h6 + alpha130;

		x0 = h0 - y0;

		x1 = h1 - y1;

		x2 = h2 - y2;

		y6 -= alpha130;

		x0 += y7;

		x3 = h3 - y3;

		x4 = h4 - y4;

		x5 = h5 - y5;

		x6 = h6 - y6;

		y6 *= scale;

		x2 += y0;

		x3 += y1;

		x4 += y2;

		x0 += y6;

		x5 += y3;

		x6 += y4;

		x2 += x3;

		x0 += x1;

		x4 += x5;

		x6 += y5;

		x2 += offset1;
		d1 = Double.doubleToRawLongBits(x2); // *(double *) &d1 = x2;

		x0 += offset0;
		d0 = Double.doubleToRawLongBits(x0); // *(double *) &d0 = x0;

		x4 += offset2;
		d2 = Double.doubleToRawLongBits(x4); // *(double *) &d2 = x4;

		x6 += offset3;
		d3 = Double.doubleToRawLongBits(x6); // *(double *) &d3 = x6;


		f0 = d0;

		f1 = d1;
		bits32 = -1;

		f2 = d2;
		bits32 >>>= 32;

		f3 = d3;
		f = f0 >>> 32;

		f0 &= bits32;
		f &= 255;

		f1 += f;
		g0 = f0 + 5;

		g = g0 >>> 32;
		g0 &= bits32;

		f = f1 >>> 32;
		f1 &= bits32;

		f &= 255;
		g1 = f1 + g;

		g = g1 >>> 32;
		f2 += f;

		f = f2 >>> 32;
		g1 &= bits32;

		f2 &= bits32;
		f &= 255;

		f3 += f;
		g2 = f2 + g;

		g = g2 >>> 32;
		g2 &= bits32;

		f4 = f3 >>> 32;
		f3 &= bits32;

		f4 &= 255;
		g3 = f3 + g;

		g = g3 >>> 32;
		g3 &= bits32;

		g4 = f4 + g;

		g4 = g4 - 4;
		s00 = key[16] & 0xff; // s00 = *(uchar *) (s + 0);

		f = g4 >> 63;
		s01 = key[17] & 0xff; // s01 = *(uchar *) (s + 1);

		f0 &= f;
		g0 &= ~f;
		s02 = key[18] & 0xff; // s02 = *(uchar *) (s + 2);

		f1 &= f;
		f0 |= g0;
		s03 = key[19] & 0xff; // s03 = *(uchar *) (s + 3);

		g1 &= ~f;
		f2 &= f;
		s10 = key[20] & 0xff; // s10 = *(uchar *) (s + 4);

		f3 &= f;
		g2 &= ~f;
		s11 = key[21] & 0xff; // s11 = *(uchar *) (s + 5);

		g3 &= ~f;
		f1 |= g1;
		s12 = key[22] & 0xff; // s12 = *(uchar *) (s + 6);

		f2 |= g2;
		f3 |= g3;
		s13 = key[23] & 0xff; // s13 = *(uchar *) (s + 7);

		s01 <<= 8;
		f0 += s00 & 0xffffffffL;
		s20 = key[24] & 0xff; // s20 = *(uchar *) (s + 8);

		s02 <<= 16;
		f0 += s01 & 0xffffffffL;
		s21 = key[25] & 0xff; // s21 = *(uchar *) (s + 9);

		s03 <<= 24;
		f0 += s02 & 0xffffffffL;
		s22 = key[26] & 0xff; // s22 = *(uchar *) (s + 10);

		s11 <<= 8;
		f1 += s10 & 0xffffffffL;
		s23 = key[27] & 0xff; // s23 = *(uchar *) (s + 11);

		s12 <<= 16;
		f1 += s11 & 0xffffffffL;
		s30 = key[28] & 0xff; // s30 = *(uchar *) (s + 12);

		s13 <<= 24;
		f1 += s12 & 0xffffffffL;
		s31 = key[29] & 0xff; // s31 = *(uchar *) (s + 13);

		f0 += s03 & 0xffffffffL;
		f1 += s13 & 0xffffffffL;
		s32 = key[30] & 0xff; // s32 = *(uchar *) (s + 14);

		s21 <<= 8;
		f2 += s20 & 0xffffffffL;
		s33 = key[31] & 0xff; // s33 = *(uchar *) (s + 15);

		s22 <<= 16;
		f2 += s21 & 0xffffffffL;

		s23 <<= 24;
		f2 += s22 & 0xffffffffL;

		s31 <<= 8;
		f3 += s30 & 0xffffffffL;

		s32 <<= 16;
		f3 += s31 & 0xffffffffL;

		s33 <<= 24;
		f3 += s32 & 0xffffffffL;

		f2 += s23 & 0xffffffffL;
		f3 += s33 & 0xffffffffL;

		out[0] = (byte)(f0 & 0xff); // *(uchar *) (out + 0) = f0;
		f0 >>>= 8; // f0 >>= 8;
		out[1] = (byte)(f0 & 0xff); // *(uchar *) (out + 1) = f0;
		f0 >>>= 8;
		out[2] = (byte)(f0 & 0xff); // *(uchar *) (out + 2) = f0;
		f0 >>>= 8;
		out[3] = (byte)(f0 & 0xff); // *(uchar *) (out + 3) = f0;
		f0 >>>= 8;
		f1 += f0;

		out[4] = (byte)(f1 & 0xff); // *(uchar *) (out + 4) = f1;
		f1 >>>= 8;
		out[5] = (byte)(f1 & 0xff); // *(uchar *) (out + 5) = f1;
		f1 >>>= 8;
		out[6] = (byte)(f1 & 0xff); // *(uchar *) (out + 6) = f1;
		f1 >>>= 8;
		out[7] = (byte)(f1 & 0xff); // *(uchar *) (out + 7) = f1;
		f1 >>>= 8;
		f2 += f1;

		out[8] = (byte)(f2 & 0xff); // *(uchar *) (out + 8) = f2;
		f2 >>>= 8;
		out[9] = (byte)(f2 & 0xff); // *(uchar *) (out + 9) = f2;
		f2 >>>= 8;
		out[10] = (byte)(f2 & 0xff); // *(uchar *) (out + 10) = f2;
		f2 >>>= 8;
		out[11] = (byte)(f2 & 0xff); // *(uchar *) (out + 11) = f2;
		f2 >>>= 8;
		f3 += f2;

		out[12] = (byte)(f3 & 0xff); // *(uchar *) (out + 12) = f3;
		f3 >>>= 8;
		out[13] = (byte)(f3 & 0xff); // *(uchar *) (out + 13) = f3;
		f3 >>>= 8;
		out[14] = (byte)(f3 & 0xff); // *(uchar *) (out + 14) = f3;
		f3 >>>= 8;
		out[15] = (byte)(f3 & 0xff); // *(uchar *) (out + 15) = f3;

		Bits.clear(key);
		return out;
	}

}
