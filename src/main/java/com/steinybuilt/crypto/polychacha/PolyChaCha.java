package com.steinybuilt.crypto.polychacha;

import com.steinybuilt.crypto.Bits;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

public class PolyChaCha {

	private final byte[] key;

	public PolyChaCha(byte[] key) {
		this.key = key;
	}

	public static byte[] generateKey() {
		byte[] key = new byte[ChaCha20.KEY_LENGTH];
		SecureRandom bytes = new SecureRandom();
		bytes.nextBytes(key);
		return key;
	}

	public final Encryption encrypt(byte[] nonce, String plaintext) {
		return encrypt(nonce, plaintext, null);
	}

	public final Encryption encrypt(byte[] nonce, String plaintext, String aad) {
		return encrypt(nonce, plaintext.getBytes(StandardCharsets.UTF_8),
				(aad == null ? null : aad.getBytes(StandardCharsets.UTF_8)));
	}

	public final Encryption encrypt(byte[] nonce, byte[] plaintext) {
		return encrypt(nonce, plaintext, null);
	}

	public final Encryption encrypt(byte[] nonce, byte[] plaintext, byte[] aad) {
		ChaCha20 cha = new ChaCha20(key);
		byte[] ciphertext = cha.encryptBytes(nonce, plaintext);
		byte[] mac = poly(cha, nonce, ciphertext, aad);
		cha.free();

		return new Encryption(nonce, aad, ciphertext, mac);
	}

	public final Encryption encryptParallel(byte[] nonce, String plaintext) {
		return encryptParallel(nonce, plaintext, null);
	}

	public final Encryption encryptParallel(byte[] nonce, String plaintext, String aad) {
		return encryptParallel(nonce, plaintext.getBytes(StandardCharsets.UTF_8),
				(aad == null ? null : aad.getBytes(StandardCharsets.UTF_8)));
	}

	public final Encryption encryptParallel(byte[] nonce, byte[] plaintext, byte[] aad) {
		ChaCha20 cha = new ChaCha20(key);
		byte[] ciphertext = cha.encryptBytesParallel(nonce, plaintext);
		byte[] mac = poly(cha, nonce, ciphertext, aad);
		cha.free();

		return new Encryption(nonce, aad, ciphertext, mac);
	}

	public final byte[] decrypt(Encryption encryption) {
		ChaCha20 cha = new ChaCha20(key);
		byte[] mac = poly(cha, encryption.getNonce(), encryption.getCiphertext(), encryption.getAad());
		if(!Poly1305.equals(mac, encryption.getMac())) {
			throw new IllegalArgumentException("failed MAC verification");
		}
		byte[] plaintext = cha.encryptBytes(encryption.getNonce(), encryption.getCiphertext());
		cha.free();
		return plaintext;
	}

	public final byte[] decryptParallel(Encryption encryption) {
		ChaCha20 cha = new ChaCha20(key);
		byte[] mac = poly(cha, encryption.getNonce(), encryption.getCiphertext(), encryption.getAad());
		if(!Poly1305.equals(mac, encryption.getMac())) {
			throw new IllegalArgumentException("failed MAC verification");
		}
		byte[] plaintext = cha.encryptBytesParallel(encryption.getNonce(), encryption.getCiphertext());
		cha.free();
		return plaintext;
	}

	public final String decryptString(Encryption encryption) {
		return new String(decrypt(encryption), StandardCharsets.UTF_8);
	}

	public final String decryptStringParallel(Encryption encryption) {
		return new String(decryptParallel(encryption), StandardCharsets.UTF_8);
	}

	private byte[] poly(ChaCha20 cha, byte[] nonce, byte[] data, byte[] aad) {
		byte[] polyKey = cha.keystream(nonce, Poly1305.KEY_LENGTH);
		Poly1305 poly = new Poly1305(polyKey);
		byte[] lengthBlock = new byte[16];

		if(aad != null && aad.length != 0) {
			int aadBlockLen = aad.length / 16 * 16;
			if(aad.length % 16 != 0) {
				aadBlockLen += 16;
			}

			byte[] aadBlock = new byte[aadBlockLen];
			for(int i = 0 ; i < aad.length ; i++) {
				aadBlock[i] = aad[i];
			}

			poly.update(aadBlock);
			Bits.longToLEBytes(lengthBlock, 0, aad.length);
		}

		poly.updateAligned(data);
		Bits.longToLEBytes(lengthBlock, 8, data.length);

		poly.update(lengthBlock);
		return poly.finish();
	}

}
