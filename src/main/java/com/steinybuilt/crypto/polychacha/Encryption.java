package com.steinybuilt.crypto.polychacha;

import java.io.Serializable;
import java.util.Arrays;

import static javax.xml.bind.DatatypeConverter.printHexBinary;

public class Encryption implements Cloneable, Serializable {

	private static final long serialVersionUID = 323023627270989557L;

	final byte[] nonce;
	final byte[] aad;
	final byte[] ciphertext;
	final byte[] mac;

	public Encryption(byte[] nonce, byte[] aad, byte[] ciphertext, byte[] mac) {
		if(nonce == null) {
			throw new NullPointerException("nonce cannot be null");
		}
		if(nonce.length != ChaCha20.NONCE_LENGTH) {
			throw new IllegalArgumentException("nonce must be " + ChaCha20.NONCE_LENGTH + " bytes");
		}

		if(ciphertext == null) {
			throw new NullPointerException("ciphertext cannot be null");
		}

		if(mac == null) {
			throw new NullPointerException("mac cannot be null");
		}
		if(mac.length != Poly1305.MAC_LENGTH) {
			throw new IllegalArgumentException("mac must be " + Poly1305.MAC_LENGTH + " bytes");
		}

		this.nonce = nonce;
		this.aad = aad;
		this.ciphertext = ciphertext;
		this.mac = mac;
	}

	@Override
	public String toString() {
		String aad = (getAad() == null ? "null" : '"' + printHexBinary(getAad()) + '"');
		return "{" +
				"\"nonce\": \"" + printHexBinary(getNonce()) + "\", " +
				"\"aad\": " + aad + ", " +
				"\"ciphertext\": \"" + printHexBinary(getCiphertext()) + "\", " +
				"\"mac\": \"" + printHexBinary(getMac()) + "\"" +
				"}";
	}

	@Override
	@SuppressWarnings("MethodDoesntCallSuperMethod")
	public Encryption clone() {
		return new Encryption(
				getNonce().clone(),
				(getAad() == null ? null : getAad().clone()),
				getCiphertext().clone(),
				getMac().clone()
		);
	}

	@Override
	public boolean equals(Object o) {
		if(this == o) {
			return true;
		}
		if(!(o instanceof Encryption)) {
			return false;
		}

		return Poly1305.equals(mac, ((Encryption)o).mac);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(mac);
	}

	public byte[] getNonce() {
		return nonce;
	}

	public byte[] getAad() {
		return aad;
	}

	public byte[] getCiphertext() {
		return ciphertext;
	}

	public byte[] getMac() {
		return mac;
	}

}
