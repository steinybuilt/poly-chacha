package com.steinybuilt.crypto.polychacha;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

final class ChaCha20 {

	public static final int KEY_LENGTH = 32;
	public static final int NONCE_LENGTH = 12;

//	private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
	private static final int BLOCK_SIZE = 64;

//	private final int parallelBlockThreshold = Optional.ofNullable(System.getProperty("parallel.block.threshold"))
//			.map(Integer::new).orElse(300);

//	private final ReadWriteLock rwLock = new ReentrantReadWriteLock(true);
	private final Lock rLock/* = rwLock.readLock()*/;
	private final Lock wLock/* = rwLock.writeLock()*/;
	private final int[] state/* = new int[12]*/;

	private boolean freed = false;

	public ChaCha20(byte[] key) {
		if(key == null) {
			throw new NullPointerException("key cannot be null");
		}
		if(key.length != KEY_LENGTH) {
			throw new IllegalArgumentException("key must be " + KEY_LENGTH + " bytes");
		}

		ReadWriteLock rwLock = new ReentrantReadWriteLock(/*true*/);
		this.rLock = rwLock.readLock();
		this.wLock = rwLock.writeLock();

		/* 256-bit key constant = "expand 32-byte k" */
		int[] state = new int[12];
		state[0] = 0x61707865;
		state[1] = 0x3320646e;
		state[2] = 0x79622d32;
		state[3] = 0x6b206574;
		/* Key setup*/
		state[4] = ((key[0] & 0xff) | ((key[1] & 0xff) << 8) | ((key[2] & 0xff) << 16) | ((key[3] & 0xff) << 24));
		state[5] = ((key[4] & 0xff) | ((key[5] & 0xff) << 8) | ((key[6] & 0xff) << 16) | ((key[7] & 0xff) << 24));
		state[6] = ((key[8] & 0xff) | ((key[9] & 0xff) << 8) | ((key[10] & 0xff) << 16) | ((key[11] & 0xff) << 24));
		state[7] = ((key[12] & 0xff) | ((key[13] & 0xff) << 8) | ((key[14] & 0xff) << 16) | ((key[15] & 0xff) << 24));
		state[8] = ((key[16] & 0xff) | ((key[17] & 0xff) << 8) | ((key[18] & 0xff) << 16) | ((key[19] & 0xff) << 24));
		state[9] = ((key[20] & 0xff) | ((key[21] & 0xff) << 8) | ((key[22] & 0xff) << 16) | ((key[23] & 0xff) << 24));
		state[10] = ((key[24] & 0xff) | ((key[25] & 0xff) << 8) | ((key[26] & 0xff) << 16) | ((key[27] & 0xff) << 24));
		state[11] = ((key[28] & 0xff) | ((key[29] & 0xff) << 8) | ((key[30] & 0xff) << 16) | ((key[31] & 0xff) << 24));

		this.state = state;
	}

	public void free() {
		wLock.lock();
		freed = true;
		for(int i = 0; i < state.length; i++) {
			state[i] = 0;
		}
		wLock.unlock();
	}

	public byte[] encryptBytes(byte[] nonce, byte[] plaintext) {
		if(nonce == null) {
			throw new NullPointerException("nonce cannot be null");
		}
		if(nonce.length != NONCE_LENGTH) {
			throw new IllegalArgumentException("nonce must be " + NONCE_LENGTH + " bytes");
		}
		if(plaintext == null) {
			throw new NullPointerException("plaintext cannot be null");
		}
		if(plaintext.length == 0) {
			return new byte[0];
		}

		byte[] ciphertext = new byte[plaintext.length];

		rLock.lock();
		try {
			if(freed) {
				throw new IllegalStateException("encryption cannot be performed after it has been freed");
			}
			encryptBlocks(1, intNonceFromByteNonce(nonce), plaintext, ciphertext, 0, plaintext.length);
		} finally {
			rLock.unlock();
		}

		return ciphertext;
	}

	public byte[] encryptBytesParallel(byte[] nonce, byte[] plaintext) {
		if(nonce == null) {
			throw new NullPointerException("nonce cannot be null");
		}
		if(nonce.length != NONCE_LENGTH) {
			throw new IllegalArgumentException("nonce must be " + NONCE_LENGTH + " bytes");
		}
		if(plaintext == null) {
			throw new NullPointerException("plaintext cannot be null");
		}
		if(plaintext.length == 0) {
			return new byte[0];
		}

		byte[] ciphertext = new byte[plaintext.length];

		rLock.lock();
		try {
			if(freed) {
				throw new IllegalStateException("encryption cannot be performed after it has been freed");
			}
			ForkJoinPool.commonPool().invoke(
					new ParallelChaCha20(intNonceFromByteNonce(nonce), plaintext, ciphertext, 1, 0, plaintext.length)
			);
		} finally {
			rLock.unlock();
		}

		return ciphertext;
	}

	public byte[] keystream(byte[] nonce, int length) {
		if(nonce == null) {
			throw new NullPointerException("nonce cannot be null");
		}
		if(nonce.length != NONCE_LENGTH) {
			throw new IllegalArgumentException("nonce must be " + NONCE_LENGTH + " bytes");
		}
		if(length < 0) {
			throw new IllegalArgumentException("length must be greater than zero");
		}
		if(length == 0) {
			return new byte[0];
		}

		byte[] stream = new byte[length];

		rLock.lock();
		try {
			if(freed) {
				throw new IllegalStateException("encryption cannot be performed after it has been freed");
			}
			encryptBlocks(0, intNonceFromByteNonce(nonce), stream, stream, 0, length);
		} finally {
			rLock.unlock();
		}

		return stream;
	}

	private void encryptBlocks(int blkIdx, int[] nonce, byte[] m, byte[] c, int offset, int len) {
		int x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15;
		int j0, j1, j2, j3, j4, j5, j6, j7, j8, j9, j10, j11, j12, j13, j14, j15;
		int[] state = this.state;
		byte[] cTarget = {};
		byte[] tmp = new byte[BLOCK_SIZE];
		int i, z, p;
		int mOffset = offset, ctOffset = 0, cOffset = offset;

		if(len == 0) {
			return;
		}

		j0 = state[0];
		j1 = state[1];
		j2 = state[2];
		j3 = state[3];
		j4 = state[4];
		j5 = state[5];
		j6 = state[6];
		j7 = state[7];
		j8 = state[8];
		j9 = state[9];
		j10 = state[10];
		j11 = state[11];
		j12 = blkIdx; // state[12];
		j13 = nonce[0]; // state[13];
		j14 = nonce[1]; // state[14];
		j15 = nonce[2]; // state[15];

		for(;;) {
			if(len < BLOCK_SIZE) {
				for(i = 0 ; i < len ; i++) tmp[i] = m[mOffset + i];
				m = tmp;
				mOffset = 0;

				cTarget = c;
				ctOffset = cOffset;

				c = tmp;
				cOffset = 0;
			}

			x0 = j0;
			x1 = j1;
			x2 = j2;
			x3 = j3;
			x4 = j4;
			x5 = j5;
			x6 = j6;
			x7 = j7;
			x8 = j8;
			x9 = j9;
			x10 = j10;
			x11 = j11;
			x12 = j12;
			x13 = j13;
			x14 = j14;
			x15 = j15;

			for(i = 0 ; i < 10 ; i++) {
				x0 += x4;
				x12 = (((z = x12 ^ x0) << 16) | (z >>> (32 - 16)));
				x8 += x12;
				x4 = (((z = x4 ^ x8) << 12) | (z >>> (32 - 12)));
				x0 += x4;
				x12 = (((z = x12 ^ x0) << 8) | (z >>> (32 - 8)));
				x8 += x12;
				x4 = (((z = x4 ^ x8) << 7) | (z >>> (32 - 7)));

				x1 += x5;
				x13 = (((z = x13 ^ x1) << 16) | (z >>> (32 - 16)));
				x9 += x13;
				x5 = (((z = x5 ^ x9) << 12) | (z >>> (32 - 12)));
				x1 += x5;
				x13 = (((z = x13 ^ x1) << 8) | (z >>> (32 - 8)));
				x9 += x13;
				x5 = (((z = x5 ^ x9) << 7) | (z >>> (32 - 7)));

				x2 += x6;
				x14 = (((z = x14 ^ x2) << 16) | (z >>> (32 - 16)));
				x10 += x14;
				x6 = (((z = x6 ^ x10) << 12) | (z >>> (32 - 12)));
				x2 += x6;
				x14 = (((z = x14 ^ x2) << 8) | (z >>> (32 - 8)));
				x10 += x14;
				x6 = (((z = x6 ^ x10) << 7) | (z >>> (32 - 7)));

				x3 += x7;
				x15 = (((z = x15 ^ x3) << 16) | (z >>> (32 - 16)));
				x11 += x15;
				x7 = (((z = x7 ^ x11) << 12) | (z >>> (32 - 12)));
				x3 += x7;
				x15 = (((z = x15 ^ x3) << 8) | (z >>> (32 - 8)));
				x11 += x15;
				x7 = (((z = x7 ^ x11) << 7) | (z >>> (32 - 7)));

				x0 += x5;
				x15 = (((z = x15 ^ x0) << 16) | (z >>> (32 - 16)));
				x10 += x15;
				x5 = (((z = x5 ^ x10) << 12) | (z >>> (32 - 12)));
				x0 += x5;
				x15 = (((z = x15 ^ x0) << 8) | (z >>> (32 - 8)));
				x10 += x15;
				x5 = (((z = x5 ^ x10) << 7) | (z >>> (32 - 7)));

				x1 += x6;
				x12 = (((z = x12 ^ x1) << 16) | (z >>> (32 - 16)));
				x11 += x12;
				x6 = (((z = x6 ^ x11) << 12) | (z >>> (32 - 12)));
				x1 += x6;
				x12 = (((z = x12 ^ x1) << 8) | (z >>> (32 - 8)));
				x11 += x12;
				x6 = (((z = x6 ^ x11) << 7) | (z >>> (32 - 7)));

				x2 += x7;
				x13 = (((z = x13 ^ x2) << 16) | (z >>> (32 - 16)));
				x8 += x13;
				x7 = (((z = x7 ^ x8) << 12) | (z >>> (32 - 12)));
				x2 += x7;
				x13 = (((z = x13 ^ x2) << 8) | (z >>> (32 - 8)));
				x8 += x13;
				x7 = (((z = x7 ^ x8) << 7) | (z >>> (32 - 7)));

				x3 += x4;
				x14 = (((z = x14 ^ x3) << 16) | (z >>> (32 - 16)));
				x9 += x14;
				x4 = (((z = x4 ^ x9) << 12) | (z >>> (32 - 12)));
				x3 += x4;
				x14 = (((z = x14 ^ x3) << 8) | (z >>> (32 - 8)));
				x9 += x14;
				x4 = (((z = x4 ^ x9) << 7) | (z >>> (32 - 7)));
			}

			x0 += j0;
			x1 += j1;
			x2 += j2;
			x3 += j3;
			x4 += j4;
			x5 += j5;
			x6 += j6;
			x7 += j7;
			x8 += j8;
			x9 += j9;
			x10 += j10;
			x11 += j11;
			x12 += j12;
			x13 += j13;
			x14 += j14;
			x15 += j15;

			p = mOffset;
			x0 ^= ((m[p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x1 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x2 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x3 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x4 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x5 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x6 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x7 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x8 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x9 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x10 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x11 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x12 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x13 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x14 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));
			x15 ^= ((m[++p] & 0xff) | ((m[++p] & 0xff) << 8) | ((m[++p] & 0xff) << 16) | ((m[++p] & 0xff) << 24));

			j12++;
			if(j12 == 0) {
				throw new ArithmeticException("encrypting more than 274,877,906,880 bytes is not supported");
			}

			p = cOffset;
			c[p] = (byte)x0;
			c[++p] = (byte)(x0 >>> 8);
			c[++p] = (byte)(x0 >>> 16);
			c[++p] = (byte)(x0 >>> 24);

			c[++p] = (byte)x1;
			c[++p] = (byte)(x1 >>> 8);
			c[++p] = (byte)(x1 >>> 16);
			c[++p] = (byte)(x1 >>> 24);

			c[++p] = (byte)x2;
			c[++p] = (byte)(x2 >>> 8);
			c[++p] = (byte)(x2 >>> 16);
			c[++p] = (byte)(x2 >>> 24);

			c[++p] = (byte)x3;
			c[++p] = (byte)(x3 >>> 8);
			c[++p] = (byte)(x3 >>> 16);
			c[++p] = (byte)(x3 >>> 24);

			c[++p] = (byte)x4;
			c[++p] = (byte)(x4 >>> 8);
			c[++p] = (byte)(x4 >>> 16);
			c[++p] = (byte)(x4 >>> 24);

			c[++p] = (byte)x5;
			c[++p] = (byte)(x5 >>> 8);
			c[++p] = (byte)(x5 >>> 16);
			c[++p] = (byte)(x5 >>> 24);

			c[++p] = (byte)x6;
			c[++p] = (byte)(x6 >>> 8);
			c[++p] = (byte)(x6 >>> 16);
			c[++p] = (byte)(x6 >>> 24);

			c[++p] = (byte)x7;
			c[++p] = (byte)(x7 >>> 8);
			c[++p] = (byte)(x7 >>> 16);
			c[++p] = (byte)(x7 >>> 24);

			c[++p] = (byte)x8;
			c[++p] = (byte)(x8 >>> 8);
			c[++p] = (byte)(x8 >>> 16);
			c[++p] = (byte)(x8 >>> 24);

			c[++p] = (byte)x9;
			c[++p] = (byte)(x9 >>> 8);
			c[++p] = (byte)(x9 >>> 16);
			c[++p] = (byte)(x9 >>> 24);

			c[++p] = (byte)x10;
			c[++p] = (byte)(x10 >>> 8);
			c[++p] = (byte)(x10 >>> 16);
			c[++p] = (byte)(x10 >>> 24);

			c[++p] = (byte)x11;
			c[++p] = (byte)(x11 >>> 8);
			c[++p] = (byte)(x11 >>> 16);
			c[++p] = (byte)(x11 >>> 24);

			c[++p] = (byte)x12;
			c[++p] = (byte)(x12 >>> 8);
			c[++p] = (byte)(x12 >>> 16);
			c[++p] = (byte)(x12 >>> 24);

			c[++p] = (byte)x13;
			c[++p] = (byte)(x13 >>> 8);
			c[++p] = (byte)(x13 >>> 16);
			c[++p] = (byte)(x13 >>> 24);

			c[++p] = (byte)x14;
			c[++p] = (byte)(x14 >>> 8);
			c[++p] = (byte)(x14 >>> 16);
			c[++p] = (byte)(x14 >>> 24);

			c[++p] = (byte)x15;
			c[++p] = (byte)(x15 >>> 8);
			c[++p] = (byte)(x15 >>> 16);
			c[++p] = (byte)(x15 >>> 24);

			if(len <= BLOCK_SIZE) {
				if(len < BLOCK_SIZE) {
					for(i = 0 ; i < len ; i++) cTarget[ctOffset + i] = c[i];
				}
				return;
			}

			len -= BLOCK_SIZE;
			cOffset += BLOCK_SIZE;
			mOffset += BLOCK_SIZE;
		}
	}

	private int[] intNonceFromByteNonce(byte[] nonce) {
		return new int[] {
				((nonce[0] & 0xff) | ((nonce[1] & 0xff) << 8) | ((nonce[2] & 0xff) << 16) | ((nonce[3] & 0xff) << 24)),
				((nonce[4] & 0xff) | ((nonce[5] & 0xff) << 8) | ((nonce[6] & 0xff) << 16) | ((nonce[7] & 0xff) << 24)),
				((nonce[8] & 0xff) | ((nonce[9] & 0xff) << 8) | ((nonce[10] & 0xff) << 16) | ((nonce[11] & 0xff) << 24))
		};
	}


	private class ParallelChaCha20 extends RecursiveAction {

		public static final long serialVersionUID = 1L;

		public final int[] nonce;
		public final byte[] m;
		public final byte[] c;
		public final int blkIdx;
		public final int offset;
		public int len;

		public ParallelChaCha20(int[] nonce, byte[] m, byte[] c, int blkIdx, int offset, int len) {
			this.nonce = nonce;
			this.m = m;
			this.c = c;
			this.blkIdx = blkIdx;
			this.offset = offset;
			this.len = len;
		}

		@Override
		protected void compute() {
			int split = len / (BLOCK_SIZE * 2);

			if(split < 300) {
				encryptBlocks(blkIdx, nonce, m, c, offset, len);
				return;
			}

			int splitBlkIdx = blkIdx + split;
			int splitOffset = offset + split * BLOCK_SIZE;

			int newLen = split * BLOCK_SIZE;
			int splitLen = len - newLen;

			invokeAll(
					new ParallelChaCha20(nonce, m, c, blkIdx, offset, newLen),
					new ParallelChaCha20(nonce, m, c, splitBlkIdx, splitOffset, splitLen)
			);
		}

	}

}
