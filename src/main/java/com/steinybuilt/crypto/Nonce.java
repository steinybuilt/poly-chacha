package com.steinybuilt.crypto;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Gets the current timestamp, and multiplies it by 1 million, making it nanoseconds. This means that, with the same
 * instance id, no more than one Nonce can be initialized within the same millisecond. Since there's only one
 * Nonce per JVM, these multiple initializations would occur in separate JVMs. This is unlikely to happen. As long
 * as it doesn't, the instance that started first, would have to increment more than once per nanosecond to catch
 * up to the second instance.
 */
public final class Nonce {

	public static final int INSTANCE_ID_LENGTH = 4;

	private static final AtomicLong nonce = new AtomicLong(System.currentTimeMillis() * 1_000_000L);

	private Nonce() {
		throw new AssertionError("No com.steinybuilt.crypto.Nonce instances for you!");
	}

	public static byte[] getNextNonce() {
		byte[] bytes = new byte[12];
		Bits.longToLEBytes(bytes, 4, nonce.incrementAndGet());
		return bytes;
	}

	@SuppressWarnings("fallthrough")
	public static byte[] getNextNonce(byte[] instId) {
		byte[] bytes = new byte[12];
		if(instId != null && instId.length != 0) {
			switch(Math.min(4, instId.length)) {
				case 4:
					bytes[3] = instId[3];
				case 3:
					bytes[2] = instId[2];
				case 2:
					bytes[1] = instId[1];
				case 1:
					bytes[0] = instId[0];
			}
		}
		Bits.longToLEBytes(bytes, 4, nonce.incrementAndGet());
		return bytes;
	}

}
