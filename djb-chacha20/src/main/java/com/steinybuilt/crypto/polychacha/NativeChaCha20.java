package com.steinybuilt.crypto.polychacha;

public class NativeChaCha20 implements AutoCloseable {

	static {
		System.loadLibrary("chacha20");
	}

	private long ctx;

	public NativeChaCha20(byte[] key) {
		init(key, 256, 64);
	}

	@Override
	public void close() {
		free();
	}

	native void init(byte[] key, int kBits, int ivBits); // kBits is 256, ivBits is 64

	native public void ivSetup(byte[] iv);

	native public void keyStreamBytes(byte[] stream, int byteLen);

	native public void encryptBytes(byte[] plaintext, byte[] ciphertext, int byteLen);
	native public void encryptBlocks(byte[] plaintext, byte[] ciphertext, int blockLen);

	native public void decryptBytes(byte[] ciphertext, byte[] plaintext, int byteLen);
	native public void decryptBlocks(byte[] ciphertext, byte[] plaintext, int blockLen);

	native public void free();

}
