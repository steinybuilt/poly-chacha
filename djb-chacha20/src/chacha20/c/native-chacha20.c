
#include <stdio.h>
#include <malloc.h>
#include <jni.h>
#include <com_steinybuilt_crypto_polychacha_NativeChaCha20.h>

//#ifndef I32T
//#define I32T long
////#define U32C(v) (v##UL)
//#endif

#ifndef I32T
#define I32T long long
#define U32C(v) (v##ULL)
#endif

#include "ecrypt-sync.h"

//#ifndef I32T
//#define I32T long
//#define U32C(v) (v##UL)
//#endif

#define GetLongF(env, self, field) (*(env))->GetLongField(env, self, field)
#define SetLongF(env, self, field, value) (*(env))->SetLongField(env, self, field, value)

#define GetByteArray(env, jArray) (*(env))->GetByteArrayElements(env, jArray, NULL)
#define GetByteArrayX(env, jArray, isCopy) (*(env))->GetByteArrayElements(env, jArray, isCopy)
#define ReleaseByteArray(env, jArray, nArray) (*(env))->ReleaseByteArrayElements(env, jArray, nArray, 0)
#define AbortByteArray(env, jArray, nArray) (*(env))->ReleaseByteArrayElements(env, jArray, nArray, JNI_ABORT)

#define GetEcryptContext(env, self) (ECRYPT_ctx *)GetLongF(env, self, NativeChaCha20_ctx)

jclass NativeChaCha20;
jfieldID NativeChaCha20_ctx;

/*
 * Signature: ([BII)V
 */
JNIEXPORT void JNICALL
Java_com_steinybuilt_crypto_polychacha_NativeChaCha20_init(JNIEnv *env, jobject self,
		jbyteArray key_j, jint kBits_j, jint ivBits_j) {
	jbyte *key;
//	jboolean isCopy;
	ECRYPT_ctx *ctx;

	NativeChaCha20 = (*env)->GetObjectClass(env, self);
	NativeChaCha20_ctx = (*env)->GetFieldID(env, NativeChaCha20, "ctx", "J");

	ECRYPT_init();

	ctx = malloc(sizeof(ECRYPT_ctx));
	SetLongF(env, self, NativeChaCha20_ctx, (jlong)ctx);

	key = GetByteArray(env, key_j/*, &isCopy*/);
//	printf("isCopy = %d\n", isCopy);

	ECRYPT_keysetup(ctx, (const u8*)key, (u32)kBits_j, (u32)ivBits_j);

//	(*env)->ReleaseByteArrayElements(env, key_j, key, 0);
	AbortByteArray(env, key_j, key);
}

/*
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL
Java_com_steinybuilt_crypto_polychacha_NativeChaCha20_ivSetup(JNIEnv *env, jobject self,
		jbyteArray iv_j) {
	jbyte *iv;
	ECRYPT_ctx *ctx;

	iv = GetByteArray(env, iv_j);

	ctx = GetEcryptContext(env, self);
	ECRYPT_ivsetup(ctx, (const u8*)iv);

	AbortByteArray(env, iv_j, iv);
}

/*
 * Signature: ([BI)V
 */
JNIEXPORT void JNICALL
Java_com_steinybuilt_crypto_polychacha_NativeChaCha20_keyStreamBytes(JNIEnv *env, jobject self,
		jbyteArray stream_j, jint byteLen_j) {
	jbyte *stream;
	ECRYPT_ctx *ctx;

	stream = GetByteArray(env, stream_j);

	ctx = GetEcryptContext(env, self);
	ECRYPT_keystream_bytes(ctx, (u8*)stream, (u32)byteLen_j);

	ReleaseByteArray(env, stream_j, stream);
}

/*
 * Signature: ([B[BI)V
 */
JNIEXPORT void JNICALL
Java_com_steinybuilt_crypto_polychacha_NativeChaCha20_encryptBytes(JNIEnv *env, jobject self,
		jbyteArray plaintext_j, jbyteArray ciphertext_j, jint byteLen_j) {
	jbyte *plaintext;
	jbyte *ciphertext;
	ECRYPT_ctx *ctx;

	plaintext = GetByteArray(env, plaintext_j);
	ciphertext = GetByteArray(env, ciphertext_j);

	ctx = GetEcryptContext(env, self);
	ECRYPT_encrypt_bytes(ctx, (const u8*)plaintext, (u8*)ciphertext, (u32)byteLen_j);

	ReleaseByteArray(env, ciphertext_j, ciphertext);
	AbortByteArray(env, plaintext_j, plaintext);
}

/*
 * Signature: ([B[BI)V
 */
JNIEXPORT void JNICALL
Java_com_steinybuilt_crypto_polychacha_NativeChaCha20_encryptBlocks(JNIEnv *env, jobject self,
		jbyteArray plaintext_j, jbyteArray ciphertext_j, jint blockLen_j) {

}

/*
 * Signature: ([B[BI)V
 */
JNIEXPORT void JNICALL
Java_com_steinybuilt_crypto_polychacha_NativeChaCha20_decryptBytes(JNIEnv *env, jobject self,
		jbyteArray ciphertext_j, jbyteArray plaintext_j, jint byteLen_j) {
	jbyte *ciphertext;
	jbyte *plaintext;
	ECRYPT_ctx *ctx;

	ciphertext = GetByteArray(env, ciphertext_j);
	plaintext = GetByteArray(env, plaintext_j);

	ctx = GetEcryptContext(env, self);
	ECRYPT_decrypt_bytes(ctx, (const u8*)ciphertext, (u8*)plaintext, (u32)byteLen_j);

	ReleaseByteArray(env, plaintext_j, plaintext);
	AbortByteArray(env, ciphertext_j, ciphertext);
}

/*
 * Signature: ([B[BI)V
 */
JNIEXPORT void JNICALL
Java_com_steinybuilt_crypto_polychacha_NativeChaCha20_decryptBlocks(JNIEnv *env, jobject self,
		jbyteArray ciphertext_j, jbyteArray plaintext_j, jint blockLen_j) {

}

/*
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_com_steinybuilt_crypto_polychacha_NativeChaCha20_free(JNIEnv *env, jobject self) {
//	ECRYPT_ctx *ctx = GetEcryptContext(env, self);
	char *ctx = (char*)GetEcryptContext(env, self);
	for(size_t i = 0, e = (sizeof(ECRYPT_ctx)) ; i < e ; ++i) {
		ctx[i] = 0;
	}
	free(ctx);
}
