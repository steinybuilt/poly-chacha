# poly-chacha
Java implementation of the ChaCha20/Poly1305 AEAD algorithm.

## Build
```
./gradlew build -x fuzzTest
```
The `fuzzTest` task is excluded above because it depends on a C compiler being
installed. If you do have a
[Gradle supported C compiler](https://docs.gradle.org/current/userguide/native_software.html#native-binaries:tool-chain-support),
feel free to remove `-x fuzzTest`. 

## Usage
**First, you need a secret key.** It MUST be a 32-byte array. Common methods
of loading the key are to parse either a Hex-string or Base64-string, from a
configuration file, environment variable, or system-property.

The JRE's XML support provides methods to parse these encodings:
* Hex: `javax.xml.bind.DatatypeConverter.parseHexBinary(String)`
* Base64: `javax.xml.bind.DatatypeConverter.parseBase64Binary(String)`

**Next, you need a nonce manager.** The nonce MUST be a 12-byte array, and
MUST be different for every message encrypted. It's not a secret, so a simple
counter is recommended. If multiple instances are running concurrently, the
first 4 bytes of the nonce SHOULD be an Instance ID, so their counters won't
produce matching nonces.

poly-chacha provides a nonce manager, `com.steinybuilt.crypto.Nonce`.

**Now, you can start encrypting things.** `PolyChaCha` accepts a nonce,
and plaintext as a string or byte-array. It also supports associated data as
strings or byte-arrays. Strings are converted to UTF-8 byte-arrays. Returned,
is an `Encryption` object, containing: the
nonce, additional associated data (aad), ciphertext, and MAC. To decrypt
ciphertext, `PolyChaCha` accepts the `Encryption` object.

**Example:**
```
import com.steinybuilt.crypto.Nonce;
import com.steinybuilt.crypto.polychacha.Encryption;
import com.steinybuilt.crypto.polychacha.PolyChaCha;

public class Example {
	private static final byte[] KEY = PolyChaCha.generateKey();
	private static final PolyChaCha AEAD = new PolyChaCha(KEY);

	public static void main(String[] args) {
		if(args.length == 0) {
			System.err.println("Please provide at lease one argument to be encrypted");
			return;
		}

		for(String arg : args) {
			Encryption encrypted = AEAD.encrypt(Nonce.getNextNonce(), arg);
			System.out.println(encrypted);
		}
	}
}
```
