package com.steinybuilt.crypto.polychacha;

public class NativePoly1305 {

	static {
		System.loadLibrary("poly1305");
	}

	private long key;

	public NativePoly1305(byte[] key) {
		this.key = init(key);
	}

	native long init(byte[] key);

	/*
	int crypto_onetimeauth_poly1305_53(unsigned char *out,
	                                   const unsigned char *in,
	                                   unsigned long long inlen,
	                                   const unsigned char *k);
	 */
	native public int cryptoOnetimeauthPoly130553(byte[] out, byte[] in, int inlen);

	/*
	int crypto_onetimeauth_poly1305_53_verify(const unsigned char *h,
	                                          const unsigned char *in,
	                                          unsigned long long inlen,
	                                          const unsigned char *k);
	 */
	native public int cryptoOnetimeauthPoly130553Verify(byte[] h, byte[] in, int inlen);

}
