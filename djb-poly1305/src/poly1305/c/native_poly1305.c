
#include <stdio.h>
#include <malloc.h>
#include <jni.h>
#include <com_steinybuilt_crypto_polychacha_NativePoly1305.h>
#include "crypto_onetimeauth_poly1305_53.h"

#define GetLongF(env, self, field) (*(env))->GetLongField(env, self, field)
#define SetLongF(env, self, field, value) (*(env))->SetLongField(env, self, field, value)

#define GetByteArray(env, jArray) (*(env))->GetByteArrayElements(env, jArray, NULL)
#define ReleaseByteArray(env, jArray, nArray) (*(env))->ReleaseByteArrayElements(env, jArray, nArray, 0)
#define AbortByteArray(env, jArray, nArray) (*(env))->ReleaseByteArrayElements(env, jArray, nArray, JNI_ABORT)

jfieldID NativePoly1305_key;

/*
 * Signature: (Ljava/nio/ByteBuffer;)V
 */
JNIEXPORT jlong JNICALL
Java_com_steinybuilt_crypto_polychacha_NativePoly1305_init(JNIEnv *env, jobject self,
		jbyteArray key_j) {
//	const unsigned char *key = (const unsigned char *)(*(env))->GetDirectBufferAddress(env, key_j);
//	jlong len = (*(env))->GetDirectBufferCapacity(env, key_j);

	jbyte *key = GetByteArray(env, key_j);
	jsize keyLen = (*(env))->GetArrayLength(env, key_j);

	unsigned char *k = calloc(sizeof(unsigned char*), (unsigned int)keyLen);
	for(jsize i = 0 ; i < keyLen ; ++i) {
		k[i] = (unsigned char)key[i];
	}

	jclass NativePoly1305 = (*env)->GetObjectClass(env, self);
	NativePoly1305_key = (*env)->GetFieldID(env, NativePoly1305, "key", "J");
//	SetLongF(env, self, NativePoly1305_key, (jlong)k);

	AbortByteArray(env, key_j, key);

	return (jlong)k;
}

/*
 * Signature: ([B[BI[B)I
 */
JNIEXPORT jint JNICALL
Java_com_steinybuilt_crypto_polychacha_NativePoly1305_cryptoOnetimeauthPoly130553(JNIEnv *env, jobject self,
		jbyteArray out_j, jbyteArray in_j, jint inlen_j) {
	jbyte *out = GetByteArray(env, out_j);
	jbyte *in = GetByteArray(env, in_j);
	jbyte *key = (jbyte*)GetLongF(env, self, NativePoly1305_key);

	jint i = crypto_onetimeauth_poly1305_53(out, in, (unsigned)inlen_j, key);

	AbortByteArray(env, in_j, in);
	ReleaseByteArray(env, out_j, out);

	return i;
 }

/*
 * Signature: ([B[BI[B)I
 */
JNIEXPORT jint JNICALL
Java_com_steinybuilt_crypto_polychacha_NativePoly1305_cryptoOnetimeauthPoly130553Verify(JNIEnv *env, jobject self,
		jbyteArray h_j, jbyteArray in_j, jint inlen_j) {
	jbyte *h = GetByteArray(env, h_j);
	jbyte *in = GetByteArray(env, in_j);
	jbyte *key = (jbyte*)GetLongF(env, self, NativePoly1305_key);

	jint i = crypto_onetimeauth_poly1305_53_verify(h, in, (unsigned)inlen_j, key);

	AbortByteArray(env, in_j, in);
	AbortByteArray(env, h_j, h);

	return i;
}

